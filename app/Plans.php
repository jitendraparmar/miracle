<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    protected $table = 'plans';

    protected $fillable = ['customer_id','protection_plus','secure','secure_plus',''];


	public $timestamps = false;
   
}
