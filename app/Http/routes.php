<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
	
	Route::get('/', 'IndexController@index');
	
	Route::post('login', 'IndexController@postLogin');
	Route::get('logout', 'IndexController@logout');
	 
	Route::get('dashboard', 'DashboardController@index');
	
	Route::get('profile', 'AdminController@profile');	
	Route::post('profile', 'AdminController@updateProfile');	
	Route::post('profile_pass', 'AdminController@updatePassword');

	
	Route::get('settings', 'SettingsController@settings');	
	Route::post('settings', 'SettingsController@settingsUpdates');	
	 //Route::controller('customers','CustomersController');
	  
	
	Route::get('users', 'UsersController@userslist');	
	Route::get('users/adduser', 'UsersController@addeditUser');	
	Route::post('users/adduser', 'UsersController@addnew');	
	Route::get('users/adduser/{id}', 'UsersController@editUser');	
	Route::get('users/delete/{id}', 'UsersController@delete');	
	
	Route::get('cancelrequest', 'CustomersController@requestlist');
    Route::post('processrequest', 'CustomersController@updatestatus');
    Route::post('deactivatecustomer', 'CustomersController@deactivatecustomer');
    Route::post('customercancelrequest', 'CustomersController@createcancelrequest');

    Route::get('customers/testach', 'CustomersController@testach');
	Route::get('customers', 'CustomersController@customers');	
	Route::get('customers/addcustomers', 'CustomersController@addcustomers');
	Route::post('customers/addcustomers', 'CustomersController@addnew');
	Route::post('customers/addnewcustomer', 'CustomersController@addnewcustomer');
	Route::post('customers/processpayments', 'CustomersController@processpayments');
	Route::post('customers/achprocesspayments', 'CustomersController@achprocesspayments');
	Route::get('customers/edit/{id}', 'CustomersController@editcustomers');
	Route::get('customers/delete/{id}', 'CustomersController@delete');	
	Route::get('customers/processmembershippayment/{id}', 'CustomersController@processmembershippayment');	
	
	Route::post('customers/export_csv', 'CustomersController@export_csv');
	Route::post('customers/export_excel', 'CustomersController@export_xls');
	Route::post('customers/export_pdf', 'CustomersController@export_pdf'); 
	
	Route::get('reports/{id}/{bystore}', 'CustomersController@reports_page');
	Route::get('reports', 'CustomersController@reports_page');
	Route::get('breakdown', 'CustomersController@breakdown_page');

	Route::get('locations', 'LocationController@locations');	
	Route::get('locations/addlocation', 'LocationController@addeditLocation'); 
	Route::get('locations/addlocation/{id}', 'LocationController@editLocation');	
	Route::post('locations/addlocation', 'LocationController@addnew');	
	Route::get('locations/delete/{id}', 'LocationController@delete');
	Route::get('locations/all_stores/{id}', 'LocationController@all_stores');

 
});

 
// Password reset link request routes...
Route::get('admin/password/email', 'Auth\PasswordController@getEmail');
Route::post('admin/password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('admin/password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('admin/password/reset', 'Auth\PasswordController@postReset');
 

 

/*Route::get('home', ['as' => 'home', 'uses' => function() {
	return view('home');
}]);*/


 Route::get('/', 'IndexController@index');
