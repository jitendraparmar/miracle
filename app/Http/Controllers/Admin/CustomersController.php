<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Customers;
use App\Storerequests;
use App\State;
use App\Location;
use App\Plans;

use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Intervention\Image\Facades\Image; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use BrandedCrate\PayJunction;

class CustomersController extends MainAdminController
{
	public function __construct()
    {
		$this->middleware('auth');
		
		parent::__construct();

		/* testing credentials */
		define('PJ_LOGIN', 'pj-ql-01');												//	Change this with Live api Login ID
		define('PJ_PASSWORD', 'pj-ql-01p');											//	Change this with Live api password
		define('PJ_APP_KEY', '42caf1ca-fc1e-4fc0-9644-d18a9ff849e9');				//	Change the value with Live Key you have got
		define('PJ_OLDAPI_URL', "https://www.payjunctionlabs.com/quick_link/");		//	remove "labs" from the url to make it live live
		define('PJ_NEWAPI_URL', "https://api.payjunctionlabs.com/transactions");	//	remove "labs" from the url to make it live url

		/* Live credentials */
//		define('PJ_LOGIN', '4b592ep');												//	Change this with Live api Login ID
//		define('PJ_PASSWORD', 'Welcome1');											//	Change this with Live api password
//		define('PJ_APP_KEY', '650a8ac8-9230-4625-acc7-7576b7cc19cc');				//	Change the value with Live Key you have got
//		define('PJ_OLDAPI_URL', "https://www.payjunction.com/quick_link/");		//	remove "labs" from the url to make it live live
//		define('PJ_NEWAPI_URL', "https://api.payjunction.com/transactions");	//	remove "labs" from the url to make it live url
         
    }
	
    public function customers()    {
//        $pj = new PayJunction\Client(array(
//			'username' => 'pj-ql-01',
//			'password' => 'pj-ql-01p',
//			'appkey'   => '42caf1ca-fc1e-4fc0-9644-d18a9ff849e9',
//			'endpoint' => 'test' // or 'live'
//		));
//
//		$response = $pj->transaction()->create(array(
//			'cardNumber' => '4444333322221111',
//			'cardExpMonth' => '01',
//			'cardExpYear' => '18',
//			'cardCvv' => '999',
//			'amountBase' => '77.55'
//		));print_r($response);die;
        if(Auth::User()->usertype!="Admin"){
            $user_id=Auth::User()->id;
            $allcustomers = Customers::WHERE('store_admin',$user_id)->orderBy('first_name')->get();
        }
        else
        {
            $allcustomers = Customers::orderBy('first_name')->get();
        } 
           
        return view('admin.pages.customers',compact('allcustomers'));
    }

	public function createcancelrequest()
	{
		$return_array['status'] = false;
		$return_array['message'] = "Something went wrong!";

		if(Auth::User()->usertype!="Admin"){
			$id = Input::Get('customer_id');
		
			// Creating instance of the Request model
			$request = new Storerequests;

			if ($request)
			{
				$request->store_admin_id = Auth::User()->id;
				$request->customer_id = $id;
				$request->status = 0;
				$request->created_at = date('Y-m-d H:i:s');
				$request->save();	// Insert new row of Cancellation request.

				$return_array['status'] = true;
				$return_array['message'] = "Request has been enrolled !";
			}
			return response()->json($return_array);
		}
		else
		{
			
		}
	}

	public function requestlist(){
       
        if(Auth::User()->usertype!="Admin"){

            \Session::flash('flash_message', 'No data found!');

            return \Redirect::back();

          //  $allcustomers = Customers::WHERE('store_admin',$user_id)->orderBy('first_name')->get();
        }
        else
        {
          $allrequests = Storerequests::leftjoin('customers', 'customer_deactivate_requests.customer_id', '=', 'customers.id')
                           ->select('customer_deactivate_requests.*', 'customers.first_name', 'customers.last_name','customers.email','customers.customer_auth_status','customer_deactivate_requests.status')
                           ->orderBy('customer_deactivate_requests.id')
                           ->get();
        } 

        return view('admin.pages.requests',compact('allrequests'));
    }

    public function updatestatus(){

		$return_array['status'] = false;
		$return_array['message'] = "Something went wrong!";

        $id = Input::Get('request_id');
		
		// Getting instance of the existing cancelation request.
		$cancel_request = Storerequests::where("id", $id)->first();
		$customer = Customers::findOrFail($cancel_request->customer_id);

		if ($cancel_request)
		{
			$cancel_request->status = 1;	//	Cancelation request has been processed.
			$cancel_request->save();	// Update the value of Status column

			$customer->customer_auth_status = 0;	//	Deactivate customer by changing the value to 0
			$customer->save();	// Update the row.

			$return_array['status'] = true;
			$return_array['message'] = "Request has been processed!";
		}
		return response()->json($return_array);
    }

	public function deactivatecustomer(){

		$return_array['status'] = false;
		$return_array['message'] = "Something went wrong!";

        $id = Input::Get('customer_id');
		
		// Getting instance of the existing cancelation request.
//		$cancel_request = Storerequests::where("id", $id)->first();
		$customer = Customers::findOrFail($id);

		if ($customer)
		{
			$customer->customer_auth_status = 0;	//	Deactivate customer by changing the value to 0
			$customer->save();	// Update the row.

			$return_array['status'] = true;
			$return_array['message'] = "The customer has been deactivated!";
		}
		return response()->json($return_array);
    }
	
    public function reports_page($bydate='',$bystore=''){
		$user_id=Auth::User()->id;
		$today = strtotime(date('Y-m-d'));
		$allcustomers = Customers::leftjoin('customer_account_id', 'customer_account_id.customer_id', '=', 'customers.id')
						->select(DB::raw('CONCAT_WS(" ",customers.first_name,customers.last_name) as name'), 'customers.product_selection','customers.id','customers.customer_auth_status', 'customers.email',DB::raw('IF(is_rechargeable_left_aid=0,"No","Yes") as is_rechargeable_left_aid'),DB::raw('IF(is_rechargeable_right_aid=0,"No","Yes") as is_rechargeable_right_aid'),'customer_account_id.account_id');
		   if($bydate == 1)
		   {
			   // This is for today only	   
			   $allcustomers->WHERE('created_at',$today);
		   }
		   if($bydate == 2)
		   {
			   // This is for current month to current day
			   $first_day_month = date('Y-m-d', strtotime('first day of ' . date('F') . ''));
			   $allcustomers->whereBetween('created_at', [strtotime($first_day_month), $today]);
		   }
		   if($bydate == 3)
		   {
			   // This is for current year to current day
			   $first_day_year = date('Y-m-d', strtotime('first day of January '.date('Y')));
			   $allcustomers->whereBetween('created_at', [strtotime($first_day_year), $today]);
		   }
		   if(!empty($bystore) && ($bystore != 'all') && ($bystore != 'undefined'))
			{
				$allcustomers = $allcustomers->WHERE('store_number',$bystore);
			}
		   if(session()->get('usertype') != 'Admin')
		   {
			   $allcustomers = $allcustomers->WHERE('store_admin',$user_id);
		   }
		   $allcustomers = $allcustomers->orderBy('customers.id','DESC')
			->get();
		   //print_r($allcustomers); die;
			//Store list
		   $stores = DB::table('location')->get();
		   $data['allcustomers'] = $allcustomers;
		   $data['stores'] = $stores;
		   $data['bydate'] = $bydate;
		   $data['bystore'] = $bystore;
		return view('admin.pages.reports',$data);
//        return view('admin.pages.customers',compact('allcustomers'));
    }

    public function breakdown_page(){
    	$first_date = date('Y-m-d',strtotime('first day of this month'));
        $last_date = date('Y-m-d',strtotime('last day of this month'));
    	$alltransactions = DB::table('transactions')->join('customers','customers.id','=','transactions.customer_id')->join('location','location.store_number','=','customers.store_number')->WHERE('status',['capture'])->whereBetween('created', [$first_date, $last_date])->orderBy('created','ASC')->get();
        return view('admin.pages.breakdown',compact('alltransactions'));
    }

    function getStateName($id){
        $state=State::WHERE('id',$id)->first();
        return $state->state_name;
    }

    function getStoreName($id){
        $state=Location::WHERE('store_number',$id)->first();
        return $state->store_name;
    }
	
    public function export_xls()
    {
		$params = Input::all();
		$jsonObj= array();
		$user_id=Auth::User()->id;
		$today = strtotime(date('Y-m-d'));
    	$allcustomers = Customers::select('customer_auth_status','gender','first_name','last_name', 'email','birth_date','address','city','state','post_code','country','telephone_number','product_selection','store_number','hearing_specialist_name','sycle_id','id_warranty_exp_left_aid','serial_number_left_aid','battery_size_left_aid','id_warranty_exp_right_aid','serial_number_right_aid','battery_size_right_aid','monthly_reoccuring');
		if($params['bydate'] == 1)
		{
			// This is for today only
			$allcustomers->WHERE('created_at',$today);
		}
		if($params['bydate'] == 2)
		{
			// This is for current month to current day
			$first_day_month = date('Y-m-d', strtotime('first day of ' . date('F') . ''));
			$allcustomers->whereBetween('created_at', [strtotime($first_day_month), $today]);
		}
		if($params['bydate'] == 3)
		{
			// This is for current year to current day
			$first_day_year = date('Y-m-d', strtotime('first day of January '.date('Y')));
			$allcustomers->whereBetween('created_at', [strtotime($first_day_year), $today]);
		}
		if(!empty($params['bystore']) && $params['bystore'] != 'all' && $params['bystore'] != 'undefined')
		{
			$allcustomers = $allcustomers->WHERE('store_number',$params['bystore']);
		}
		if(!empty($params['chk_customer']))
		{
			$allcustomers = $allcustomers->whereIn('id',$params['chk_customer']);
		}
		if(session()->get('usertype') != 'Admin')
		{
			$allcustomers = $allcustomers->where('store_admin',$user_id);
		}
		$allcustomers = $allcustomers->get();
		foreach($allcustomers as $customers)
        {
            if($customers->customer_auth_status==1)
            {
                $row['customer_auth_status'] = 'Active'; 
            } 
            else
            {
                $row['customer_auth_status'] = 'Canceled'; 
            }
            $row['gender'] = $customers->gender;
            $row['first_name'] = $customers->first_name;
            $row['last_name'] = $customers->last_name;
            $row['email'] = $customers->email;
			if($customers->birth_date !== '0000-00-00')
				$row['birth_date'] = date('m-d-Y',strtotime($customers->birth_date));
            else
				$row['birth_date'] = 'N/A';
			$row['address'] = $customers->address;
            $row['city'] = $customers->city;
            $row['state'] = $this->getStateName($customers->state);
            $row['post_code'] = $customers->post_code;
            $row['country'] = $customers->country;
            $row['telephone_number'] = $customers->telephone_number;
            $row['product_selection'] = $customers->product_selection;
			if($customers->store_number != '')
				$row['store_number'] = $this->getStoreName($customers->store_number);
			else
				$row['store_number'] = 'N/A';
            $row['hearing_specialist_name'] = $customers->hearing_specialist_name;
            $row['sycle_id'] = $customers->sycle_id;
            $row['id_warranty_exp_left_aid'] = date('m-d-Y',$customers->id_warranty_exp_left_aid);
            $row['serial_number_left_aid'] = $customers->serial_number_left_aid;
            $row['battery_size_left_aid'] = $customers->battery_size_left_aid;
            $row['id_warranty_exp_right_aid'] = date('m-d-Y',$customers->id_warranty_exp_right_aid);
            $row['serial_number_right_aid'] = $customers->serial_number_right_aid;
            $row['battery_size_right_aid'] = $customers->battery_size_right_aid;
            $row['monthly_reoccuring'] = $customers->monthly_reoccuring;
            
            array_push($jsonObj,$row);
        }
		$filename = time().'customers';
		Excel::create($filename, function($excel) use($jsonObj) {
			$excel->sheet('Sheet 1', function($sheet) use($jsonObj) {
			$sheet->fromArray($jsonObj);
			$sheet->setColumnFormat(array('F' => 'mm-dd-yyyy','J' => 'General','J' => 'General','M' => '0.00',));
			$sheet->row(1, array('Status','Gender','First Name', 'Last Name','Email','Birth Date','Address','City','State','Post Code','Country','Telephone Number','Product Selection','Store','Hearing Specialist','Sycle ID','Left Aid Warranty Expiration','Left Aid Serial','Left Aid Battery Size','Right Aid Warranty Expiration','Right Aid Serial','Right Aid Battery Size','Monthly Reoccuring'));
			$sheet->row(1, function($row) {    // call cell manipulation methods
			$row->setFontWeight('bold');
			});
		});
			
		})->store('xls',public_path('upload/export/xls'), true);
		$return['fullurl'] = url('upload/export/xls').'/'.$filename.".xls";
		$return['file'] = $filename.".xls";
		return $return;
    }

    public function export_csv()
    {
    	$jsonObj= array();
		$params = Input::all();
		$user_id=Auth::User()->id;
		$today = strtotime(date('Y-m-d'));
    	$allcustomers = Customers::select('customer_auth_status','gender','first_name','last_name', 'email','birth_date','address','city','state','post_code','country','telephone_number','product_selection','store_number','hearing_specialist_name','sycle_id','id_warranty_exp_left_aid','serial_number_left_aid','battery_size_left_aid','id_warranty_exp_right_aid','serial_number_right_aid','battery_size_right_aid','monthly_reoccuring');
		if($params['bydate'] == 1)
		{
			// This is for today only
			$allcustomers->WHERE('created_at',$today);
		}
		if($params['bydate'] == 2)
		{
			// This is for current month to current day
			$first_day_month = date('Y-m-d', strtotime('first day of ' . date('F') . ''));
			$allcustomers->whereBetween('created_at', [strtotime($first_day_month), $today]);
		}
		if($params['bydate'] == 3)
		{
			// This is for current year to current day
			$first_day_year = date('Y-m-d', strtotime('first day of January '.date('Y')));
			$allcustomers->whereBetween('created_at', [strtotime($first_day_year), $today]);
		}
		if(!empty($params['bystore']) && $params['bystore'] != 'all')
		{
			$allcustomers = $allcustomers->WHERE('store_number',$params['bystore']);
		}
		if(!empty($params['chk_customer']))
		{
			$allcustomers = $allcustomers->whereIn('id',$params['chk_customer']);
		}
		if(session()->get('usertype') != 'Admin')
		{
			$allcustomers = $allcustomers->where('store_admin',$user_id)->get();
		}
		$allcustomers = $allcustomers->get();
        foreach($allcustomers as $customers)
        {
            if($customers->customer_auth_status==1)
            {
                $row['customer_auth_status'] = 'Active'; 
            } 
            else
            {
                $row['customer_auth_status'] = 'Canceled'; 
            }  
            $row['gender'] = $customers->gender;
            $row['first_name'] = $customers->first_name;
            $row['last_name'] = $customers->last_name;
            $row['email'] = $customers->email;            
            if($customers->birth_date != '0000-00-00')
				$row['birth_date'] = date('m-d-Y',strtotime($customers->birth_date));
            else
				$row['birth_date'] = 'N/A';
            $row['address'] = $customers->address;
            $row['city'] = $customers->city;
            $row['state'] = !empty($customers->state) ? $this->getStateName($customers->state) : '';
            $row['post_code'] = $customers->post_code;
            $row['country'] = $customers->country;
            $row['telephone_number'] = $customers->telephone_number;
            $row['product_selection'] = $customers->product_selection;
            if($customers->store_number != '')
				$row['store_number'] = $this->getStoreName($customers->store_number);
			else
				$row['store_number'] = 'N/A';
            $row['hearing_specialist_name'] = $customers->hearing_specialist_name;
            $row['sycle_id'] = $customers->sycle_id;
            $row['id_warranty_exp_left_aid'] = date('m-d-Y',$customers->id_warranty_exp_left_aid);
            $row['serial_number_left_aid'] = $customers->serial_number_left_aid;
            $row['battery_size_left_aid'] = $customers->battery_size_left_aid;
            $row['id_warranty_exp_right_aid'] = date('m-d-Y',$customers->id_warranty_exp_right_aid);
            $row['serial_number_right_aid'] = $customers->serial_number_right_aid;
            $row['battery_size_right_aid'] = $customers->battery_size_right_aid;
            $row['monthly_reoccuring'] = $customers->monthly_reoccuring;
            
            array_push($jsonObj,$row);
        }
        $filename = time().'customers';
		Excel::create($filename, function($excel) use($jsonObj) {
                $excel->sheet('Sheet 1', function($sheet) use($jsonObj) {
                $sheet->fromArray($jsonObj);
                $sheet->setColumnFormat(array('F' => 'mm-dd-yyyy','J' => 'General','J' => 'General','M' => '0.00',));
                $sheet->row(1, array('Status','Gender','First Name', 'Last Name','Email','Birth Date','Address','City','State','Post Code','Country','Telephone Number','Product Selection','Store','Hearing Specialist','Sycle ID','Left Aid Warranty Expiration','Left Aid Serial','Left Aid Battery Size','Right Aid Warranty Expiration','Right Aid Serial','Right Aid Battery Size','Monthly Reoccuring'));
                $sheet->row(1, function($row) {    // call cell manipulation methods
                $row->setFontWeight('bold');
                });  
            });
        })->store('csv',public_path('upload/export/csv/'), true);
		$return['fullurl'] = url('upload/export/csv').'/'.$filename.".csv";
		$return['file'] = $filename.".csv";
		return $return;
    }

    public function export_pdf()
    {
        $jsonObj= array();
		$params = Input::all();
		$user_id=Auth::User()->id;
		$today = strtotime(date('Y-m-d'));
		$allcustomers = Customers::select('customer_auth_status','gender','first_name','last_name', 'email','birth_date','address','city','state','post_code','country','telephone_number','product_selection','store_number','hearing_specialist_name','sycle_id','id_warranty_exp_left_aid','serial_number_left_aid','battery_size_left_aid','id_warranty_exp_right_aid','serial_number_right_aid','battery_size_right_aid','monthly_reoccuring');
    	if($params['bydate'] == 1)
		{
			// This is for today only
			$allcustomers->WHERE('created_at',$today);
		}
		if($params['bydate'] == 2)
		{
			// This is for current month to current day
			$first_day_month = date('Y-m-d', strtotime('first day of ' . date('F') . ''));
			$allcustomers->whereBetween('created_at', [strtotime($first_day_month), $today]);
		}
		if($params['bydate'] == 3)
		{
			// This is for current year to current day
			$first_day_year = date('Y-m-d', strtotime('first day of January '.date('Y')));
			$allcustomers->whereBetween('created_at', [strtotime($first_day_year), $today]);
		}
		if(!empty($params['bystore']) && $params['bystore'] != 'all')
		{
			$allcustomers = $allcustomers->WHERE('store_number',$params['bystore']);
		}
		if(!empty($params['chk_customer']))
		{
			$allcustomers = $allcustomers->whereIn('id',$params['chk_customer']);
		}
		if(session()->get('usertype') != 'Admin')
		{
			$allcustomers = $allcustomers->where('store_admin',$user_id);
		}
		$allcustomers = $allcustomers->get();
		foreach($allcustomers as $customers)
        {
            if($customers->customer_auth_status==1)
            {
                $row['customer_auth_status'] = 'Active'; 
            } 
            else
            {
                $row['customer_auth_status'] = 'Canceled'; 
            }  
            $row['gender'] = $customers->gender;
            $row['first_name'] = $customers->first_name;
            $row['last_name'] = $customers->last_name;
            $row['email'] = $customers->email;            
            if($customers->birth_date !== '0000-00-00')
				$row['birth_date'] = date('m-d-Y',strtotime($customers->birth_date));
            else
				$row['birth_date'] = 'N/A';
            $row['address'] = $customers->address;
            $row['city'] = $customers->city;
            $row['state'] = !empty($customers->state) ? $this->getStateName($customers->state) : '';
            $row['post_code'] = $customers->post_code;
            $row['country'] = $customers->country;
            $row['telephone_number'] = $customers->telephone_number;
            $row['product_selection'] = $customers->product_selection;
            if($customers->store_number != '')
				$row['store_number'] = $this->getStoreName($customers->store_number);
			else
				$row['store_number'] = 'N/A';
            $row['hearing_specialist_name'] = $customers->hearing_specialist_name;
            $row['sycle_id'] = $customers->sycle_id;
            $row['id_warranty_exp_left_aid'] = date('m-d-Y',$customers->id_warranty_exp_left_aid);
            $row['serial_number_left_aid'] = $customers->serial_number_left_aid;
            $row['battery_size_left_aid'] = $customers->battery_size_left_aid;
            $row['id_warranty_exp_right_aid'] = date('m-d-Y',$customers->id_warranty_exp_right_aid);
            $row['serial_number_right_aid'] = $customers->serial_number_right_aid;
            $row['battery_size_right_aid'] = $customers->battery_size_right_aid;
            $row['monthly_reoccuring'] = $customers->monthly_reoccuring;
            
            array_push($jsonObj,$row);
        }
		$filename = time().'customers';
        Excel::create($filename, function($excel) use($jsonObj) {
                $excel->sheet('Sheet 1', function($sheet) use($jsonObj) {
                $sheet->fromArray($jsonObj);
                $sheet->setColumnFormat(array('F' => 'mm-dd-yyyy','J' => 'General','J' => 'General','M' => '0.00',));
                $sheet->row(1, array('Status','Gender','First Name', 'Last Name','Email','Birth Date','Address','City','State','Post Code','Country','Telephone Number','Product Selection','Store','Hearing Specialist','Sycle ID','Left Aid Warranty Expiration','Left Aid Serial','Left Aid Battery Size','Right Aid Warranty Expiration','Right Aid Serial','Right Aid Battery Size','Monthly Reoccuring'));
                $sheet->row(1, function($row) {    // call cell manipulation methods
                $row->setFontWeight('bold');
                $row->setBorder('none', 'none', 'none', 'none');
                });  
            });
         })->store('pdf',public_path('upload/export/pdf/'), true);
		$return['fullurl'] = url('upload/export/pdf').'/'.$filename.".pdf";
		$return['file'] = $filename.".pdf";
		return $return;
    }
     
    public function addcustomers()
    {
        $allstate = State::orderBy('id')->get();
        $locations = Location::orderBy('store_name')->get();
        return view('admin.pages.addeditcustomersnew',compact('allstate','locations'));
    } 

    public function addnew($request)
    {
        $data =  \Input::except(array('_token')) ;
        
        $inputs = $request->all();print_r($inputs);die;
        
        if(!empty($inputs['id']))
        {
            $rule=array(
                'gender' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state' => 'required',
                'post_code' => 'required',
                'telephone_number' => 'required',
                'hearing_specialist_name' => 'required'
                );
        }
        else
        {
            $rule=array(
                'gender' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state' => 'required',
                'post_code' => 'required',
                'telephone_number' => 'required',
                'hearing_specialist_name' => 'required'              
                 );
        }
        
        $validator = \Validator::make($data,$rule);
 
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->messages());
        }
          
        if(!empty($inputs['id'])){
           
            $customers = Customers::findOrFail($inputs['id']);

            $plans = Plans::WHERE('customer_id',$inputs['id'])->first();

        }else{

            $customers = new Customers;

            $plans = new Plans;

            $customers->created_at = strtotime(Carbon::today()->toDateString());

            $plans->date = strtotime(Carbon::today()->toDateString());

        }

        if($inputs['product_selection']==14.95 or $inputs['product_selection']==179.40)
        {
            $plans->protection_plus = 1;
            $plans->secure = 0;
            $plans->secure_plus = 0;
        }
        elseif ($inputs['product_selection']==21.95 or $inputs['product_selection']==14.45 or $inputs['product_selection']==263.40 or $inputs['product_selection']==173.40) {
           
           $plans->secure = 1;
           $plans->protection_plus = 0;
           $plans->secure_plus = 0;
        }
        else
        {
            $plans->secure_plus = 1;
            $plans->secure = 0;
           $plans->protection_plus = 0;
        }

         $plans->save();
        
    
        $customers->store_admin = Auth::User()->id;
        $customers->customer_auth_status = $inputs['customer_auth_status'];
        $customers->gender = $inputs['gender'];
        $customers->first_name = $inputs['first_name']; 
        $customers->last_name = $inputs['last_name'];
        $customers->birth_date = strtotime($inputs['birth_date']);
        $customers->email = $inputs['email'];

        $customers->address = $inputs['address'];
        $customers->city = $inputs['city'];
        $customers->state = $inputs['state'];
        $customers->post_code = $inputs['post_code'];
        $customers->country = $inputs['country'];

        $customers->telephone_number = $inputs['telephone_number'];

        $customers->product_selection = $inputs['product_selection'];

        $customers->store_number = $inputs['store_number'];

        $customers->hearing_specialist_name = $inputs['hearing_specialist_name'];
        $customers->sycle_id = $inputs['sycle_id']; 
        $customers->id_warranty_exp_left_aid = strtotime($inputs['id_warranty_exp_left_aid']);
        $customers->serial_number_left_aid = $inputs['serial_number_left_aid'];
        $customers->battery_size_left_aid = $inputs['battery_size_left_aid'];
        $customers->id_warranty_exp_right_aid = strtotime($inputs['id_warranty_exp_right_aid']);
        $customers->serial_number_right_aid = $inputs['serial_number_right_aid'];
        $customers->battery_size_right_aid = $inputs['battery_size_right_aid'];       
        $customers->monthly_reoccuring = $inputs['monthly_reoccuring'];
        $customers->save();
		
		$account_id = getAccountId($customers->id);
        
        if(!empty($inputs['id'])){
			getBillingFrequency($customers->id, $inputs["billing_frequency"], "update"); 

            \Session::flash('flash_message', 'Changes Saved');

            return \Redirect::back();
        }
		else
		{
			getBillingFrequency($customers->id, $inputs["billing_frequency"], "new"); 
			\Session::flash('flash_message', 'Added');
			return \Redirect::to('admin/customers/edit/'.$customers->id);
        }
    }

	public function addnewcustomer(Request $request)
	{
		$data = $request->all();
//		Add customer in Customers table
		$return_info = Self::savenewcustomer($request);

		$data = $request->all();
		if($return_info['status'] == 1 && $data['id'] != ''){

			\Session::flash('flash_message', 'Information Updated!');
			return \Redirect::to('admin/customers/edit/'.$data['id']);

        }else if($return_info['status'] == 1 && $data['id'] == ''){

			\Session::flash('flash_message', 'Information saved !');
            return redirect('admin/customers/addcustomers');
        }
		else
		{
			\Session::flash('flash_message', 'Something went wrong!');
			return redirect('admin/customers/addcustomers');
		}
//		Add plan in Plans table
//		Charge first installment charge for the customer and save it in Transactions table
//		Set recurring transactions charge for the customer in PayJunction
	}

	private function savenewcustomer($request_data)
	{
		$data =  \Input::except(array('_token')) ;
        
        $inputs = $request_data->all();
        
        if(!empty($inputs['id']))
        {
            $rule=array(
                'gender' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'address' => 'required',
                'city' => 'required',
//                'state' => 'required',
                'post_code' => 'required',
                'telephone_number' => 'required',
                'hearing_specialist_name' => 'required'
			);
        }
        else
        {
            $rule=array(
                'gender' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'address' => 'required',
                'city' => 'required',
//                'state' => 'required',
                'post_code' => 'required',
                'telephone_number' => 'required',
                'hearing_specialist_name' => 'required'
            );
        }

        $validator = \Validator::make($data,$rule);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->messages());
        }
		if(!empty($inputs['id']))
		{
            $customers = Customers::findOrFail($inputs['id']);
            $plans = Plans::WHERE('customer_id',$inputs['id'])->first();
        }
		else
		{
            $customers = new Customers;
            $plans = new Plans;
            $customers->created_at = strtotime(Carbon::today()->toDateString());
            $plans->date = strtotime(Carbon::today()->toDateString());
        }
		$customers->store_admin = Auth::User()->id;
        $customers->customer_auth_status = $inputs['customer_auth_status'];
        $customers->gender = $inputs['gender'];
        $customers->first_name = $inputs['first_name'];
        $customers->last_name = $inputs['last_name'];
		if($inputs['birth_date'] != '')
			$customers->birth_date = date('Y-m-d',strtotime($inputs['birth_date']));
        $customers->email = $inputs['email'];
        $customers->address = $inputs['address'];
        $customers->city = $inputs['city'];
        $customers->state = $inputs['state'];
        $customers->post_code = $inputs['post_code'];
        $customers->country = 'United States';
        $customers->telephone_number = $inputs['telephone_number'];
        $customers->product_selection = $inputs['product_selection'];
        $customers->store_number = $inputs['store_number'];
        $customers->hearing_specialist_name = $inputs['hearing_specialist_name'];
        $customers->sycle_id = $inputs['sycle_id'];
        $customers->id_warranty_exp_left_aid = strtotime($inputs['id_warranty_exp_left_aid']);
        $customers->serial_number_left_aid = $inputs['serial_number_left_aid'];
        $customers->battery_size_left_aid = $inputs['battery_size_left_aid'];
		if(isset($inputs['is_rechargeable_left_aid']))
			$customers->is_rechargeable_left_aid = $inputs['is_rechargeable_left_aid'];
        $customers->id_warranty_exp_right_aid = strtotime($inputs['id_warranty_exp_right_aid']);
        $customers->serial_number_right_aid = $inputs['serial_number_right_aid'];
        $customers->battery_size_right_aid = $inputs['battery_size_right_aid'];
		if(isset($inputs['is_rechargable_right_aid']))
			$customers->is_rechargeable_right_aid = $inputs['is_rechargeable_right_aid'];
        $customers->monthly_reoccuring = $inputs['billing_frequency'];

        $customers->billing_frequency = $inputs['billing_frequency'];
		if($inputs['billing_frequency'] == 1)
			$customers->installments = $inputs['monthly_duration'];
		elseif ($inputs['billing_frequency'] == 2)
			$customers->installments = $inputs['yearly_duration'];

		if($inputs['recurring_date'] != '')
		{
			$customers->billing_date = date('Y-m-',strtotime('now')).$inputs['recurring_date'];
		}
        $customers->save();
		/* Do not need to process payment in Customer Edit mode */
		if(!empty($inputs['id'])){
			$return_array['status'] = true;
			return $return_array;
		}
        if($inputs['product_selection']==14.95)
        {
            $plans->protection_plus = 1;
            $plans->secure = 0;
            $plans->secure_plus = 0;
        }
        elseif ($inputs['product_selection']==21.95 or $inputs['product_selection']==14.45)
		{   
			$plans->secure = 1;
			$plans->protection_plus = 0;
			$plans->secure_plus = 0;
        }
        else
        {
            $plans->secure_plus = 1;
            $plans->secure = 0;
			$plans->protection_plus = 0;
        }
		if(isset($customers->id) && !empty($customers->id))
		{
			$plans->customer_id = $customers->id;
		}
		if(isset($plans->store_admin_id) && !empty($plans->store_admin_id))
		{
			$plans->store_admin_id = Auth::User()->id;
		}
		$plans->save();
		
		if($inputs['cardNumber'] != '' && $inputs['cardExpMonth'] != '')
		{
			$account_id = getAccountId($customers->id);
			$customers->cardNumber = $inputs['cardNumber'];
			$customers->cardExpMonth = $inputs['cardExpMonth'];
			$customers->cardExpYear = $inputs['cardExpYear'];
			$customers->cardCvv = $inputs['cardCvv'];
			$customers->storeName = $inputs['store_name'];
			/* Now let's charge the customer for product */
			$payment_details = Self::processcardpayment($customers,$account_id,$customers->storeName);
		}
		elseif($inputs['routingNumber'] != '' && $inputs['accNumber'] != '')
		{
			$account_id = getAccountId($customers->id);
			$amount = '';
			$routingNumber = $inputs['routingNumber'];
			$accNumber = $inputs['accNumber'];

			if($inputs['billing_frequency'] == 1)
			{
				// Monthly recurring
				$amount = ($inputs['product_selection'] * $inputs['monthly_duration']);
				$amount = "$".(number_format((float)$amount, 2, ".", ""));
			}
			elseif($inputs['billing_frequency'] == 2)
			{
				// Yearly recurring
				$amount = ($inputs['product_selection'] * $inputs['yearly_duration'] * 12);
				$amount = "$".(number_format((float)$amount, 2, ".", ""));
			}
			$payment_details = self::curlachtransaction($amount,$routingNumber,$accNumber,$inputs);
//			$result = achpayments($customers->product_selection,$account,$routing);
		}

		return $payment_details;
	}

	private function processcardpayment($customer_data,$account_id,$store_name)
	{
		// Making recurring schedule add in PayJunction API (Quicklink v1.2) (OLD API of PAYJUNCTION)
	
		// Unlike the newer REST API, credential info is sent in the POST data, no the HTTP header
		$dc_logon = PJ_LOGIN;
		$dc_password = PJ_PASSWORD;

		// Basic setup options, you shouldn't have to change these at all
		$dc_version = "1.2";
		$dc_transaction_type = "AUTHORIZATION_CAPTURE";	

		// Schedule to setup: Run for $1.00, every month, on the first of the month starting October 1st 2016. End after 10 approved transactions

		// Use the card on file based off the transaction id (vault id is not recognized in the Quicklink v1.2 API)
		$dc_transaction_id = "12037";

		/* if you need to send full card info because you don't have a transaction id to use the following is accepted:*/

		// Optional 
		$dc_first_name = $customer_data['first_name'];
		$dc_last_name = $customer_data['last_name'];
		$dc_address = $customer_data['address'];
		$dc_city = $customer_data['city'];
		$dc_state = $customer_data['state'];
		$dc_zipcode = $customer_data['post_code'];

		// Required
//		$dc_number = '4444333322221111';
//		$dc_expiration_month = '5';
//		$dc_expiration_year = '2020';
//		$dc_verification_number = '999';
		$dc_number = $customer_data['cardNumber'];
		$dc_expiration_month = $customer_data['cardExpMonth'];
		$dc_expiration_year = $customer_data['cardExpYear'];
		$dc_verification_number = $customer_data['cardCvv'];

		// Recurring amount (how much to charge on _each_ recurring transaction)
		$dc_transaction_amount = $customer_data['product_selection'];

		// Run every month (1 for every month, 2 for every other month, 3 for every third month, etc.)
		if($customer_data['billing_frequency'] == 1)
		{
			/* Call the API function to execute the first transaction */
			$first_paid = self::firstpaymentexecution($customer_data,$account_id,$store_name);

			// If the numbers of installments are more than 1 (As we have charged one above) set recurring transaction
			// for remaining installments
			if($customer_data['installments'] > 1)
			{
				$dc_schedule_periodic_number = "1";
				$dc_schedule_periodic_type = "month";
				// Stop schedule after 'n' approved transactions
				$dc_schedule_limit = ($customer_data['installments'] - 1);
				// Tell the API we're creating a recurring schedule
				$dc_schedule_create = "true";
				// Run on the first of the month, starting October 1st, 2016
				$day = date('d',strtotime($customer_data->billing_date));
				$dc_schedule_start = date('Y-m-d', mktime(0, 0, 0, date('m')+1, $day, date('Y')));
				$data_array_schedule = array(
												"dc_logon" => $dc_logon,
												"dc_password" => $dc_password,
												"dc_first_name" => $dc_first_name,
												"dc_last_name" => $dc_last_name,
												"dc_address" => $dc_address,
												"dc_city" => $dc_city,
												"dc_state" => $dc_city,
												"dc_zipcode" => $dc_zipcode,

												// Required
												"dc_number" => $dc_number,
												"dc_expiration_month" => $dc_expiration_month,
												"dc_expiration_year" => $dc_expiration_year,
												"dc_verification_number" => $dc_verification_number,
												"dc_transaction_amount" => $dc_transaction_amount,

												"dc_schedule_create" => $dc_schedule_create,
												"dc_schedule_periodic_number" => $dc_schedule_periodic_number,
												"dc_schedule_periodic_type" => $dc_schedule_periodic_type,
												"dc_schedule_start" => $dc_schedule_start,
												"dc_schedule_limit" => $dc_schedule_limit
											);
				$set_recurring = self::paymentexecution($data_array_schedule);
			}
		}
		elseif($customer_data['billing_frequency'] == 2)
		{
			// Multiply the amount by 12 months for yearly option
			$dc_transaction_amount = $customer_data['product_selection'] * 12;

			// Create the array and use http_build_query to get the final string
			$data_array_single = array(
				"dc_logon" => $dc_logon,
				"dc_password" => $dc_password,
				"dc_first_name" => $dc_first_name,
				"dc_last_name" => $dc_last_name,
				"dc_address" => $dc_address,
				"dc_city" => $dc_city,
				"dc_state" => $dc_city,
				"dc_zipcode" => $dc_zipcode,

				// Required
				"dc_number" => $dc_number,
				"dc_expiration_month" => $dc_expiration_month,
				"dc_expiration_year" => $dc_expiration_year,
				"dc_verification_number" => $dc_verification_number,
				"dc_transaction_amount" => $dc_transaction_amount,
			);
			/* Call the API function to execute the transaction */
			$first_paid = self::firstpaymentexecution($customer_data,$account_id,$store_name);

			// If the numbers of installments are more than 1 (As we have charged one above) set recurring transaction
			// for remaining installments
			if($customer_data['installments'] > 1)
			{
				$dc_schedule_periodic_number = "12";
				$dc_schedule_periodic_type = "month";
				// Stop schedule after 'n' approved transactions
				$dc_schedule_limit = ($customer_data['installments'] - 1);
				// Tell the API we're creating a recurring schedule
				$dc_schedule_create = "true";
				// Run on the first of the month, starting October 1st, 2016
				$day = date('d',strtotime($customer_data->billing_date));
				$dc_schedule_start = date('Y-m-d', mktime(0, 0, 0, date('m'), $day, date('Y')+1));
				$data_array_schedule = array_merge($data_array_single,array(
																"dc_schedule_create" => $dc_schedule_create,
																"dc_schedule_periodic_number" => $dc_schedule_periodic_number,
																"dc_schedule_periodic_type" => $dc_schedule_periodic_type,
																"dc_schedule_start" => $dc_schedule_start,
																"dc_schedule_limit" => $dc_schedule_limit)
											);
				$set_recurring = self::paymentexecution($data_array_schedule);
			}
		}
		$return_array['status'] = true;
//		$return_array['transactionId'] = $result['transactionId'];
		return $return_array;
	}

	private function paymentexecution($data_array)
	{
		$data_encoded = http_build_query($data_array);

		// Initialize cURL...

		// cURL handle object
		$ch = curl_init();

		// Sending to sandbox (payjunctionlabs.com) Notice the endpoint is different than what is used by the REST API
		curl_setopt($ch, CURLOPT_URL, PJ_OLDAPI_URL);

		// General setup...
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_encoded);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'X-PJ-Application-Key: '.PJ_APP_KEY,
		'Accept: application/json'
		));

		// Required: Set the Content-Type header to X-WWW-FORM-URLENCODED!
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type" => "x-www-form-urlencoded"));

		// Execute the cURL request

		$response = curl_exec($ch);
		$curl_errno = curl_errno($ch);
		$curl_error = curl_error($ch);
		curl_close($ch);

		if ($curl_errno) {print_r('PayJunction Error (CustomerController Line 956): ');print_r($curl_error);die;
			// Handle the error, if any
		} else {
			// Result will be a key value pairs separated by a special character
			$response_array = array();
			$response_vars = explode(chr(28), $response);



			while ($key_value = next($response_vars))

			{

				list($key, $value) = explode("=", $key_value);

				$response_array[$key] = $value;

			}

			return $response_array;
			// IMPORTANT! Whether you are running the first transaction immediately or not the card will be checked with a $1.00 test transaction
			// Therefore if our response code is 00 or 85 the schedule will be saved in PJ

//			if (strcmp($response_array['dc_response_code'], "00") == 0 || strcmp ($response['dc_response_code'], "85") == 0)
//
//			{
//
//				// Success! Do things
//				log_successful_recurring_add($response_array['dc_transaction_id']);
//
//			}
//
//			else
//
//			{
//				// Had an error
//				log_unsuccessful_recurring_add(array('Error code: {$response_array["dc_response_code"]} - {$response_array["dc_response_message"]})'));
//
//			}
		}
	}

	private function firstpaymentexecution($input_data,$account_id,$store_name)
	{
		$state_name = 'N/A';
		if(isset($input_data['state']) && !empty($input_data['state']))
		{
			$state_name_data = State::find($input_data['state']);
			$state_name = $state_name_data['state_name'];
		}
		$amount = number_format((float)$input_data['product_selection'], 2, ".", "");
		if($input_data['billing_frequency'] == 2)
		{
			$amount = $input_data['product_selection'] * 12;
			$amount = number_format((float)$amount, 2, ".", "");
		}
//		print_r($amount);die;
		$login = PJ_LOGIN;
		$password = PJ_PASSWORD;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,PJ_NEWAPI_URL);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'X-PJ-Application-Key: '.PJ_APP_KEY,
			'Accept: application/json'
			));

		curl_setopt($ch,CURLOPT_POSTFIELDS,
				"billingFirstName=".$input_data['first_name']."&billingLastName=".$input_data['last_name'].
				"&billingPhone=".$input_data['telephone_number']."&billingEmail=".$input_data['email'].
				"&billingCompanyName=".$store_name."&invoiceNumber=".$account_id."&billingAddress=".$input_data['address'].
				"&billingZip=".$input_data['post_code']."&billingCountry=".$input_data['country'].
				"&billingCity=".$input_data['city']."&billingState=".$state_name."&cardNumber=".$input_data['cardNumber'].
				"&cardExpMonth=".$input_data['cardExpMonth']."&cardExpYear=".$input_data['cardExpYear'].
				"&cardCvv=".$input_data['cardCvv']."&action=CHARGE&amountBase=".$amount);


		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = json_decode(curl_exec ($ch), true);

		curl_close ($ch);

		$return_array = array();
		if(isset($result['response']) && $result['response']['code'] == "00")
		{
			$return_array['status'] = true;
			$return_array['transactionId'] = $result['transactionId'];
		}
		else
		{
			$return_array['status'] = false;
		}

		return $return_array;
	}

    public function editcustomers($id)
    {
        $customer = Customers::findOrFail($id);
        $allstate = State::orderBy('id')->get();
        $locations = Location::orderBy('store_name')->get();
		$processMembership = "";
        $store_admin_id=$customer->store_admin;

		$account_id = getAccountId($id);

		$cPay_id = DB::table('customer_payments')->select('id', 'pj_customer_id')-> where('customer_id', $customer['id'])->get();;

		$billing_frequency = getBillingFrequency($id);
		if(count($cPay_id) > 0) {
			//Show A Process Membership Payment Button
			$processMembership = getCardOnFile($id);
		}


			
          if(Auth::User()->usertype!="Admin" and Auth::User()->id!=$store_admin_id){

            \Session::flash('flash_message', 'No data found!');

            return \Redirect::back();
            
        }else{
          
          
          return view('admin.pages.addeditcustomersnew',compact('customer','allstate','locations','processMembership','account_id','billing_frequency'));
        }
    }    
    
    public function delete($id)
    {
         $customers = Customers::findOrFail($id);

        $store_admin_id=$customers->store_admin;

        if(Auth::User()->usertype!="Admin" and Auth::User()->id!=$store_admin_id){

            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
            
        }
          
        $customers->delete();

        \Session::flash('flash_message', 'Deleted');

        return redirect()->back();

    }
   
   public function fetch_all_customers()
   {
//	   DROP DATABASE IF EXISTS tutorial_database
//	   DROP DATABASE TUTORIALS
   }
   
   
   
function processmembershippayment($id) {
	   
	 
$customer = Customers::findOrFail($id);
$store_id = DB::table('location')->select('store_name')-> where('store_number', $customer['store_number'])->get();
$store_name=$store_id[0]->store_name;		   
		
	
$transaction_id = DB::table('customer_payments')->select('transaction_id')-> where('customer_id', $customer['id'])->get();;


$transaction_id = $transaction_id[0]->transaction_id;




$result = chargeCard(null, $customer, true);


if($result["status"] == "DECLINED") {
	

	 \Session::flash('flash_message', 'Credit Card Declined.');
	  
	 return redirect()->back();

}

else if($result["status"] == "CAPTURE"){
	
	storeTransaction($result, $customer, $store_name);
	alertPayment($result, $customer);

	\Session::flash('flash_message', 'Credit Card Charge Successful.');

	 return redirect()->back();
	
}

else {
	
	
	\Session::flash('flash_message', 'There was a problem charging this card. Payment status set to ' . $result["status"]);

	 return redirect()->back();
	
}

   }
   


   
   function processpayments() {
       $input = \Input::all();//$input['cardExpMonth'] = '01';
	   $id = $input['id'];
	   $customer = Customers::findOrFail($id);
$store_id = DB::table('location')->select('store_name')-> where('store_number', $customer['store_number'])->get();
$store_name=$store_id[0]->store_name;
	 
	   if($input['type'] == 'charge') {
		   
		   
  $result = chargeCard($input, $customer);
		
	//print_r($result); exit;	
		
if($result["status"] == "DECLINED") {
	

	 \Session::flash('flash_message', 'Credit Card Declined.');
	  
	 return redirect()->back();

}

else {
	
	storeTransaction($result, $customer,$store_name);
	alertPayment($result, $customer);
	\Session::flash('flash_message', 'Credit Card Charge Successful.');

	 return redirect()->back();
	
}


	   }

	 	else if($input['type'] == 'update') {
			
			
			 updateCard($input, $customer);
}

   }
   
   
  function achprocesspayments() {
	$input = \Input::all();
	$id = $input['id'];
	$customer = Customers::findOrFail($id);
	$store_id = DB::table('location')->select('store_name')-> where('store_number', $customer['store_number'])->get();
	$acconts = DB::table('customer_account_id')->select('account_id')-> where('customer_id', $customer['id'])->get();
	$store_name=$store_id[0]->store_name;
	$account=$acconts[0]->account_id;
	
			if($input['accNumber']=='')
			{
				\Session::flash('flash_message', 'Please Enter Account Number');
				return redirect()->back();	
			}
			else{
				$account=$input['accNumber'];
			}
			if($input['routingNumber']=='')
			{
				\Session::flash('flash_message', 'Please Enter Routing Number');
				return redirect()->back();	
			}
			else{
				$routing=$input['routingNumber'];
			}	
		
       $result = achpayments($customer,$account,$routing);
	
	if( $result['response']['message'] ='Approved'){
		\Session::flash('flash_message', 'Payment successfull!!');
		storeTransaction($result, $customer,$store_name);
	alertPayment($result, $customer);
	\Session::flash('flash_message', 'Payment Successfully.');

	 return redirect()->back();
		
	}else{
		\Session::flash('flash_message', 'Payment declined!!');
		return redirect()->back();
	}
	
   }
  	
   /*	Function: curlachtransaction()
	*	Purpose: TO make transaction with ACH details
	*	Date: 19-09-2016
	*	Developed by: Himanshu Upadhyay
	*/
	private function curlachtransaction($amount,$route,$account,$input_data)
    {
		$login = PJ_LOGIN;
		$password = PJ_PASSWORD;
		$achRoutingNumber = $route;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,PJ_NEWAPI_URL);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'X-PJ-Application-Key: '.PJ_APP_KEY,
			'Accept: application/json'
			));


		curl_setopt($ch,CURLOPT_POSTFIELDS,"billingFirstName=".$input_data['first_name']."&billingLastName=".$input_data['last_name']."&billingPhone=".$input_data['telephone_number']."&billingEmail".$input_data['email']."&billingCompanyName".$input_data['store_number']."&billingAddress".$input_data['address']."&billingCity".$input_data['city']."&achRoutingNumber=".$achRoutingNumber."&achAccountNumber=".$account."&achAccountType=CHECKING&achType=PPD&amountBase=".$amount);


		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = json_decode(curl_exec ($ch), true);

		curl_close ($ch);

		$return_array = array();
		if(isset($result['response']) && $result['response']['code'] == "00")
		{
			$return_array['status'] = true;
			$return_array['transactionId'] = $result['transactionId'];
		}
		else
		{
			$return_array['status'] = false;
		}

		return $return_array;
		// To see entire response from API.
//		print_r($result);die;
    }

   /*	Function: makeachtransaction()
	*	Purpose: TO make transaction with ACH details
	*	Date: 16-09-2016
	*	Developed by: Himanshu Upadhyay
	*/
	private function makeachtransaction($amount,$route,$account)
    {
		$pj = new PayJunction\Client(array(
			'username' => 'pj-ql-01',
			'password' => 'pj-ql-01p',
			'appkey'   => '42caf1ca-fc1e-4fc0-9644-d18a9ff849e9',
			'endpoint' => 'test' // or 'live'
		));
		$response = $pj->transaction()->create(array(
			'achRoutingNumber' => $route,
            'achAccountNumber' => $account,
            'achAccountType' => 'CHECKING',
            'achType' => 'PPD',
            'amountBase' => $amount
		));
		print_r($response);die;
    }



//END OF CLASS 

}

function achpayments($customer,$account,$routing){

	$login = "pj-ql-01";
	$password = "pj-ql-01p";
	//$login = "4b592ep";
	//$password = "Welcome1";
	$achRoutingNumber=$routing;	
	$amount = rand (1,100);
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,"https://api.payjunction.com/transactions");
	curl_setopt($ch, CURLOPT_USERPWD, "pj-ql-01:pj-ql-01p");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'X-PJ-Application-Key: d0f6388e-4ae7-4774-af8f-33f067899da1',
	//    'X-PJ-Application-Key: d0f6388e-4ae7-4774-af8f-33f067899da1',
		'Accept: application/json'
		));


	curl_setopt($ch,CURLOPT_POSTFIELDS,"achRoutingNumber=".$achRoutingNumber."&achAccountNumber=".$account."&achAccountType=CHECKING&achType=PPD&amountBase=".$customer);


	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$result = json_decode(curl_exec ($ch), true);

	curl_close ($ch);

	print_r($result);die;
	$response = $result["response"];

	return $result;

	}

	function chargeCard($input = null, $customer, $auto = false) {

		$login = "pj-ql-01";
		$password = "pj-ql-01p";

		//$amount = $customer['product_selection'];

		//FOR TESTING TO PREVENT DUPLICATE TRANSACTIONS
		$amount = rand (1,100);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://api.payjunctionlabs.com/transactions");
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'X-PJ-Application-Key: 8b9cb320-c486-41af-b0a2-729600a8aa63',
			'Accept: application/json'
			));

		if($auto) {

			$transaction_id = DB::table('customer_payments')->select('transaction_id')-> where('customer_id', $customer['id'])->get();;


		$transaction_id = $transaction_id[0]->transaction_id;


		curl_setopt($ch, CURLOPT_POSTFIELDS,
					"action=CHARGE&transactionId=".$transaction_id."&amountBase=".$amount);
		}

		else {
		curl_setopt($ch, CURLOPT_POSTFIELDS,
					"cardNumber=".$input['cardNumber']."&cardExpMonth=".$input['cardExpMonth']."&cardExpYear=".$input['cardExpYear']."&amountBase=".$amount);
		}


		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = json_decode(curl_exec ($ch), true); 

		curl_close ($ch);


		$response = $result["response"];

		return $result;
	}


	function storeTransaction($transaction, $customer,$store_name=null) {

		//Insert the transaction
		$transaction_id = DB::table('transactions')->insert(
			array('customer_id' => $customer['id'], 'pj_transaction_id' => $transaction['transactionId'], 'amount'=> $transaction['amountTotal'], 'created' => $transaction['created'], 'lastModified' =>  $transaction['lastModified'], 'status' => $transaction['status'], 'full_result' => json_encode($transaction))
		);

		//update the customers card on file
		$cPay_id = DB::table('customer_payments')->select('id', 'pj_customer_id')-> where('customer_id', $customer['id'])->get();

		if(count($cPay_id) > 0) {
			
		//customer has an account, just update the information
		$updatePJ  = updatePayJunctionCustomer($customer,$cPay_id[0]->pj_customer_id,$store_name);

			DB::table('customer_payments')
					->where('customer_id', $customer['id'])
					->update(array('transaction_id' => $transaction['transactionId']));
		}
		else {
			//customer does not have an account

			$pjCustomer = newPayJunctionCustomer($customer,$store_name);

			if(!array_key_exists("errors", $pjCustomer)){
			DB::table('customer_payments')->insert(
				array('customer_id' => $customer['id'], 'transaction_id' => $transaction['transactionId'], 'pj_customer_id' => $pjCustomer['customerId'])
			);}
		}

		return true;
	}



	function newPayJunctionCustomer($customer,$store_name=null) {
		$login = "pj-ql-01";
		$password = "pj-ql-01p";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://api.payjunctionlabs.com/customers");
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
												'X-PJ-Application-Key: 8b9cb320-c486-41af-b0a2-729600a8aa63',
												'Accept: application/json'
												));
		curl_setopt($ch, CURLOPT_POSTFIELDS,"firstName=".$customer['first_name']."&lastName=".$customer['last_name']."&email=".$customer['email']."&phone=".$customer['telephone_number']."&identifier=".$customer['id']."&companyName=".$store_name);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = json_decode(curl_exec ($ch), true);
		curl_close ($ch);

		return $result;

	}


	function updatePayJunctionCustomer($customer, $pj_customer_id, $store_name=null) {

		$login = "pj-ql-01";
		$password = "pj-ql-01p";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://api.payjunctionlabs.com/customers/".$pj_customer_id);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		//curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
												'X-PJ-Application-Key: 8b9cb320-c486-41af-b0a2-729600a8aa63',
												'Accept: application/json'
												));
		curl_setopt($ch, CURLOPT_POSTFIELDS,"firstName=".$customer['first_name']."&lastName=".$customer['last_name']."&email=".$customer['email']."&phone=".$customer['telephone_number']."&identifier=".$customer['id']."&companyName=".$store_name);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = json_decode(curl_exec ($ch), true);
		curl_close ($ch);

		return $result;
	}


	function getBillingFrequency($customer_id, $frequency = NULL, $type = NULL) {

		if ($frequency != NULL && $type == "update") {
		//update the frequency	
			DB::table('customer_billing_frequency')
				->where('customer_id', $customer_id)
				->update(array('frequency' => $frequency));
		} else if ($type == "new") {
			$query = DB::table('customer_billing_frequency')->insert(
				array('customer_id' => $customer_id, 'frequency' => $frequency));
		} else {
			$frequency = DB::table('customer_billing_frequency')->select('frequency')
					->where('customer_id', $customer_id)->get();

			if (isset($frequency[0])) {
				$frequency = $frequency[0]->frequency;
			} else {
				$frequency = DB::table('customer_billing_frequency')->insert(
					array('customer_id' => $customer_id, 'frequency' => NULL));

				$frequency = $frequency["frequency"];
			}
		}
		return $frequency;
	}

function getCardOnFile($customer_id) {

		$trans_id = DB::table('customer_payments')->select('id', 'transaction_id')-> where('customer_id', $customer_id)->get();
		$trans_id = $trans_id[0]->transaction_id;
		$card_info =  DB::table('transactions')->select('full_result')-> where('pj_transaction_id', $trans_id)->get();
		$card_info = json_decode($card_info[0]->full_result, true);

		$card_info =  $card_info["vault"];
		$processMembership =  '<b>'.$card_info["accountType"] . ' ending in ' .$card_info["lastFour"] .' </b> on file. <a href="../processmembershippayment/'.$customer_id.'" class="btn btn-default waves-effect waves-light m-l-5" style="display:inline;">Process Membership Payment </a><br /><br />';

		return $processMembership;
	}


	function alertPayment($transaction, $customer) {

		require $_SERVER['DOCUMENT_ROOT'].'/miracle/public/cron/sendmail.php';

		$body = "Thank you for your Miracle Ear Protection Plan purchase. \n\n Please see your transaction details below.";
		$cardData = $transaction['vault'];
		$paymentMethod = $cardData['accountType'] . " ending in " . $cardData['lastFour'];
		$to = $customer['email'];
		$subject = "Miracle Ear Protection Plan Purchase";

		$divider = "\n-----------------------------\n\n";
		$email = $body . "\n" . $divider. $customer['first_name'] . " ". $customer['last_name']."\n$". $customer['product_selection']." - " . $customer["monthly_reoccuring"] . " Month Reoccuring \n".$paymentMethod . " \nTransaction ID: " . $transaction['transactionId'];

		SendMail($to,$subject,$email);
}



function getAccountId($customer_id) {
	
	$account_id = DB::table('customer_account_id')->select('account_id')-> where('customer_id', $customer_id)->get();

	if(isset($account_id[0])) {
		if($account_id[0]->account_id) {
			$account_id = $account_id[0]->account_id;
		}
	}
	else {
		//insert a new account id
		$account_id = DB::table('customer_account_id')->insertGetId(
		array('customer_id' => $customer_id, 'account_id' => NULL));

//		$account_id = $account_id["account_id"];
	}
	return $account_id;
}