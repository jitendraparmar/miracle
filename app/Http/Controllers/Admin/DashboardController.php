<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use App\Location;
use App\Customers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends MainAdminController {

	public function __construct() {
		$this->middleware('auth');
		$user_id=Auth::User()->id;
		$userdata = DB::table('users')->select('first_name','last_name','email','usertype')->where('id',$user_id)->first();
		session()->put('first_name', $userdata->first_name);
		session()->put('last_name', $userdata->last_name);
		session()->put('email', $userdata->email);
		session()->put('usertype', $userdata->usertype);
	}

	function protection_plus_month($month_name, $plan_name) {
		$user_id=Auth::User()->id;
		$first_day_month = date('Y-m-d', strtotime('first day of ' . $month_name . ''));
		$last_day_month = date('Y-m-d', strtotime('last day of ' . $month_name . ''));
		
		if(session()->get('usertype') != 'Admin')
		{
			$total_rec_month = DB::table('plans')
			->select(DB::raw('SUM(' . $plan_name . ') as total_plans'))
			->where('store_admin_id',$user_id)
			->whereBetween('date', [strtotime($first_day_month), strtotime($last_day_month)])
			->get();
		}
		else
		{
			$total_rec_month = DB::table('plans')
			->select(DB::raw('SUM(' . $plan_name . ') as total_plans'))
			->whereBetween('date', [strtotime($first_day_month), strtotime($last_day_month)])
			->get();
		}
		foreach ($total_rec_month as $key => $object_month) {
			$total_month_plan = $object_month->total_plans;
		}

		if ($total_month_plan) {
			return $total_month_plan;
		} else {
			return 0;
		}
	}

	public function index() {
		$user_id=Auth::User()->id;
		$first_date = date('Y-m-d', strtotime('first day of this month'));
		$last_date = date('Y-m-d', strtotime('last day of this month'));
		$firstday = date('Y-m-d H:i:s', strtotime('first day of this month'));
		$lastday = date('Y-m-d H:i:s', strtotime('last day of this month'));
		
		if(session()->get('usertype') != 'Admin')
		{
			$total_revanue = DB::table('transactions')
			->select(DB::raw('SUM(amount) as total_sales'))
			->leftjoin('customers','customers.id', '=', 'transactions.customer_id')
			->where('status', 'CAPTURE')
			->where('store_admin',$user_id)
			->whereBetween(DB::raw('created'), [gmdate("Y-m-d\TH:i:s\Z",strtotime($firstday)),gmdate("Y-m-d\TH:i:s\Z",strtotime($lastday))])
			->get();
		}
		else
		{
			$total_revanue = DB::table('transactions')
			->select(DB::raw('SUM(amount) as total_sales'))
			->leftjoin('customers','customers.id', '=', 'transactions.customer_id')
			->where('status', 'CAPTURE')
			->whereBetween(DB::raw('created'), [gmdate("Y-m-d\TH:i:s\Z",strtotime($firstday)),gmdate("Y-m-d\TH:i:s\Z",strtotime($lastday))])
			->get();
		}
		foreach ($total_revanue as $key => $object) {
			$all_revanue = $object->total_sales;
		}
		
		$today_date = strtotime(Carbon::today()->toDateString());
		if(session()->get('usertype') != 'Admin')
		{
			$active_customers = Customers::where('customer_auth_status', '1')->where('store_admin',$user_id)->count();
			$today_customers = Customers::where('created_at', $today_date)->where('store_admin',$user_id)->count();
			$new_customers = Customers::whereBetween('created_at', [strtotime($first_date), strtotime($last_date)])->where('store_admin',$user_id)->count();
			$active_plans = Customers::where('customer_auth_status','1')->where('store_admin',$user_id)->count();
			$this_month_plans = Customers::whereBetween('created_at', [strtotime($first_date), strtotime($last_date)])->where('store_admin',$user_id)->count();
			$total_protection = DB::table('plans')
			 ->select(DB::raw('SUM(protection_plus) as protection_plus'))
			 ->where('store_admin_id',$user_id)
			 ->whereBetween('date', [strtotime($first_date), strtotime($last_date)])
			 ->get();
		}
		else
		{
			$active_customers = Customers::where('customer_auth_status', '1')->count();
			$today_customers = Customers::where('created_at', $today_date)->count();
			$new_customers = Customers::whereBetween('created_at', [strtotime($first_date), strtotime($last_date)])->count();
			$active_plans = Customers::where('customer_auth_status','1')->count();
			$this_month_plans = Customers::whereBetween('created_at', [strtotime($first_date), strtotime($last_date)])->count();
			$total_protection = DB::table('plans')
			 ->select(DB::raw('SUM(protection_plus) as protection_plus'))
			 ->whereBetween('date', [strtotime($first_date), strtotime($last_date)])
			 ->get();
		}
		foreach ($total_protection as $key => $protection_object) {
			$total_protection_plus = $protection_object->protection_plus;
		}
		
		if(session()->get('usertype') != 'Admin')
		{
			$total_secure = DB::table('plans')
			 ->select(DB::raw('SUM(secure) as secure'))
			 ->where('store_admin_id',$user_id)
			 ->whereBetween('date', [strtotime($first_date), strtotime($last_date)])
			 ->get();
		}
		else
		{
			$total_secure = DB::table('plans')
			 ->select(DB::raw('SUM(secure) as secure'))
			 ->whereBetween('date', [strtotime($first_date), strtotime($last_date)])
			 ->get();
		}
		foreach ($total_secure as $key => $secure_object) {
			$total_secure_plan = $secure_object->secure;
		}
		
		if(session()->get('usertype') != 'Admin')
		{
			$total_secure_plus = DB::table('plans')
			 ->select(DB::raw('SUM(secure_plus) as secure_plus'))
			 ->where('store_admin_id',$user_id)
			 ->whereBetween('date', [strtotime($first_date), strtotime($last_date)])
			 ->get();
		}
		else
		{
			$total_secure_plus = DB::table('plans')
			 ->select(DB::raw('SUM(secure_plus) as secure_plus'))
			 ->whereBetween('date', [strtotime($first_date), strtotime($last_date)])
			 ->get();
		}
		foreach ($total_secure_plus as $key => $secure_plus_object) {
			$total_secure_plus_plan = $secure_plus_object->secure_plus;
		}
		
		//Graph DATA
		$total_protection_plus_jan = $this->protection_plus_month('January', 'protection_plus');
		$total_secure_jan = $this->protection_plus_month('January', 'secure');
		$total_secure_plus_jan = $this->protection_plus_month('January', 'secure_plus');

		$total_protection_plus_feb = $this->protection_plus_month('February', 'protection_plus');
		$total_secure_feb = $this->protection_plus_month('February', 'secure');
		$total_secure_plus_feb = $this->protection_plus_month('February', 'secure_plus');

		$total_protection_plus_mar = $this->protection_plus_month('March', 'protection_plus');
		$total_secure_mar = $this->protection_plus_month('March', 'secure');
		$total_secure_plus_mar = $this->protection_plus_month('March', 'secure_plus');

		$total_protection_plus_apr = $this->protection_plus_month('April', 'protection_plus');
		$total_secure_apr = $this->protection_plus_month('April', 'secure');
		$total_secure_plus_apr = $this->protection_plus_month('April', 'secure_plus');

		$total_protection_plus_may = $this->protection_plus_month('May', 'protection_plus');
		$total_secure_may = $this->protection_plus_month('May', 'secure');
		$total_secure_plus_may = $this->protection_plus_month('May', 'secure_plus');

		$total_protection_plus_jun = $this->protection_plus_month('June', 'protection_plus');
		$total_secure_jun = $this->protection_plus_month('June', 'secure');
		$total_secure_plus_jun = $this->protection_plus_month('June', 'secure_plus');

		$total_protection_plus_jul = $this->protection_plus_month('July', 'protection_plus');
		$total_secure_jul = $this->protection_plus_month('July', 'secure');
		$total_secure_plus_jul = $this->protection_plus_month('July', 'secure_plus');

		$total_protection_plus_aug = $this->protection_plus_month('August', 'protection_plus');
		$total_secure_aug = $this->protection_plus_month('August', 'secure');
		$total_secure_plus_aug = $this->protection_plus_month('August', 'secure_plus');

		$total_protection_plus_sep = $this->protection_plus_month('September', 'protection_plus');
		$total_secure_sep = $this->protection_plus_month('September', 'secure');
		$total_secure_plus_sep = $this->protection_plus_month('September', 'secure_plus');

		$total_protection_plus_oct = $this->protection_plus_month('October', 'protection_plus');
		$total_secure_oct = $this->protection_plus_month('October', 'secure');
		$total_secure_plus_oct = $this->protection_plus_month('October', 'secure_plus');
		
		$total_protection_plus_nov = $this->protection_plus_month('November', 'protection_plus');
		$total_secure_nov = $this->protection_plus_month('November', 'secure');
		$total_secure_plus_nov = $this->protection_plus_month('November', 'secure_plus');

		$total_protection_plus_dec = $this->protection_plus_month('December', 'protection_plus');
		$total_secure_dec = $this->protection_plus_month('December', 'secure');
		$total_secure_plus_dec = $this->protection_plus_month('December', 'secure_plus');

		return view('admin.pages.dashboard', compact('all_revanue', 'active_customers', 'today_customers', 'new_customers', 'active_plans', 'this_month_plans', 'total_protection_plus', 'total_secure_plan', 'total_secure_plus_plan', 'total_protection_plus_jan', 'total_secure_jan', 'total_secure_plus_jan', 'total_protection_plus_feb', 'total_secure_feb', 'total_secure_plus_feb', 'total_protection_plus_mar', 'total_secure_mar', 'total_secure_plus_mar', 'total_protection_plus_apr', 'total_secure_apr', 'total_secure_plus_apr', 'total_protection_plus_may', 'total_secure_may', 'total_secure_plus_may', 'total_protection_plus_jun', 'total_secure_jun', 'total_secure_plus_jun', 'total_protection_plus_jul', 'total_secure_jul', 'total_secure_plus_jul', 'total_protection_plus_aug', 'total_secure_aug', 'total_secure_plus_aug', 'total_protection_plus_sep', 'total_secure_sep', 'total_secure_plus_sep', 'total_protection_plus_oct', 'total_secure_oct', 'total_secure_plus_oct', 'total_protection_plus_nov', 'total_secure_nov', 'total_secure_plus_nov', 'total_protection_plus_dec', 'total_secure_dec', 'total_secure_plus_dec'));
	}

}
