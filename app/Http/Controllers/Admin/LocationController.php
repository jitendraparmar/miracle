<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use App\Location;
use App\State;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use Intervention\Image\Facades\Image; 


class LocationController extends MainAdminController
{
	public function __construct()
    {
		 $this->middleware('auth');
		  
		parent::__construct(); 	
		  
    }
    public function locations()    { 
        
              
        $locations = Location::orderBy('store_name')->get();
        
        if(Auth::User()->usertype!="Admin"){

            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
            
        }
 
         
        return view('admin.pages.locations',compact('locations'));
    }
    
    public function addeditLocation()    { 
        
        $allstate = State::orderBy('id')->get();

        if(Auth::User()->usertype!="Admin"){

            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
            
        }

        

        return view('admin.pages.addeditlocation',compact('allstate'));
    }
    
    public function addnew(Request $request)
    { 
        
        $data =  \Input::except(array('_token')) ;
        
        $rule=array(
                'store_name' => 'required',
                'store_number' => 'required',
                'store_address' => 'required',
                'store_city' => 'required',
                'store_state' => 'required',
                'store_zip' => 'required'                
                 );
        
         $validator = \Validator::make($data,$rule);
 
        if ($validator->fails())
        {
                return redirect()->back()->withErrors($validator->messages());
        } 
        $inputs = $request->all();
        
        if(!empty($inputs['id'])){
           
            $location = Location::findOrFail($inputs['id']);

        }else{

            $location = new Location;

        }
         
        
        $location->store_name = $inputs['store_name']; 
        $location->store_number = $inputs['store_number']; 
        $location->store_address = $inputs['store_address']; 
        $location->store_city = $inputs['store_city']; 
        $location->store_state = $inputs['store_state']; 
        $location->store_zip = $inputs['store_zip']; 
         
         
        
         
        $location->save();
        
        if(!empty($inputs['id'])){

            \Session::flash('flash_message', 'Changes Saved');

            return \Redirect::back();
        }else{

            \Session::flash('flash_message', 'Added');

            return \Redirect::back();

        }            
        
         
    }     
   
    
    public function editLocation($location_id)    
    {     
        $allstate = State::orderBy('id')->get();

    	  if(Auth::User()->usertype!="Admin"){

            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
            
        }
        	     
          $location = Location::findOrFail($location_id);
          
           

          return view('admin.pages.addeditlocation',compact('location','allstate'));
        
    }	 
    
    public function delete($location_id)
    {
    	if(Auth::User()->usertype=="Admin")
        {
        	
        $location = Location::findOrFail($location_id);
        $location->delete();

        \Session::flash('flash_message', 'Deleted');

        return redirect()->back();
        }
        else
        {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
            
        
        }
    }

	public function all_stores($location_id)
	{
		if(strlen($location_id) == 13)
        {
			$location = getlocation($location_id);
//			$location->delete();

			\Session::flash('flash_message', 'Location saved');

			return redirect()->back();
        }
        else
        {
            \Session::flash('flash_message', 'Access denied!');
            return redirect('admin/dashboard');
        }
	}

     
     
    	
}
