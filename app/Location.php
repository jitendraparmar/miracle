<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'location';

    protected $fillable = ['store_name', 'store_number', 'store_address', 'store_city', 'store_state','store_zip'];


	public $timestamps = false;

	public static function getStoreInfo($id) 
    { 
        $location=Location::find($id);

        return $location;
    }
   
}
