<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_admin','customer_auth_status','gender', 'first_name','last_name','birth_date'];

     public $timestamps = false;   
     
    public static function getUserInfo($id) 
    { 
        return Customers::find($id);
    }

    public static function getUserFullname($id) 
    { 
        $userinfo=Customers::find($id);

        return $userinfo->first_name.' '.$userinfo->last_name;
    }
}
