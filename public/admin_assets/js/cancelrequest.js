$(document).ready(function(){
   
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

   $( ".processed" ).click(function() {

        var $this = $(this);
        var confirmed = confirm("Have you stopped recurring account for this customer from PayJunction virtual terminal ?")

        if(confirmed)
        {
            var request_id = $(".processed").attr('data-bind');

            var token;
            token = $('input[name=_token]').val();
            $.ajax({
                url: "processrequest",
                data: {request_id : request_id},
                type :'post',
    //            headers: {'X-CSRF-TOKEN': token},
                success: function(data){
                    if(data.status)
                    {
                        $this.parents('tr:first').find('td:eq(3)').text('Completed');
                        $this.parents('tr:first').find('td:eq(5)').text('--');
                    }
            }});
        }
    });

});