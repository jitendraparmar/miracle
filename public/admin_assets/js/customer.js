$(document).ready(function(){
   
    var selected_product = '';
    var product_name = '';
    var product_value = '';
    $('.aid-select-radio-row').hide();

    if($("input[name='product_selection']:checked").length > 0)
    {
        product_value = $("input[name='product_selection']:checked").val();
        selected_product = $("input[name='product_selection']:checked").next('label').text()+" $"+$("input[name='product_selection']:checked").val();
        $('.selected-product-class').html(" <i class='fa fa-check'></i> "+selected_product+" / month");
        $('.product-val').text("$"+product_value+" x ");

        /* Check if selected product is for SINGLE AID ? */
        var product_id = $(this).attr('id');
        if(product_id == "radio3" || product_id == "radio5")
        {
            $('.aid-select-radio-row').show();
        }
    }

    $('.product-radio').click(function(){
        product_name = $(this).next('label').text();
        product_value = $(this).val();
        selected_product = " <i class='fa fa-check'></i> "+product_name+" $"+product_value+" / month";
        $('.selected-product-class').html(selected_product);
        $('.product-val').text("$"+product_value+" x ");

        /* Check if selected product is for SINGLE AID ? */
        var product_id = $(this).attr('id');
        if(product_id == "radio3" || product_id == "radio5")
        {
            $('.aid-select-radio-row').show();
        }
        else
        {
            $('.aid-select-radio-row').hide();
            $('.left-aid-form').show();
            $('.right-aid-form').show();
        }
    });

//    $('.monthly-plan').hide();
    var duration = 0;
    var plan_type = 0;
    var total_cost = 0;
    var installment_cost = 0;
    $('.yearly-plan').hide();
    $('.plan-radio').click(function(){
        var plan_value = $(this).val();
        if(plan_value == 1)
        {
            $('.monthly-plan').show();
            $('.yearly-plan').hide();
        }
        else if(plan_value == 2)
        {
            $('.yearly-plan').show();
            $('.monthly-plan').hide();
        }
        $('.total-cost').text('');
        $('.duration').val('');
        $('.duration').focus();
        duration = 0;
        plan_type = 0;
        total_cost = 0;
        installment_cost = 0;

    });  

    $('.duration').keyup(function(){
        duration = $(this).val();
        plan_type = $("input[name='billing_frequency']:checked").val();
        total_cost = 0;
        installment_cost = 0;

        if(plan_type == 1 && duration > 0)
        {
            total_cost = duration * product_value;
            total_cost = Number(total_cost).toFixed(2);
            installment_cost = Number(product_value).toFixed(2);
            $('.total-cost').text(" = $"+installment_cost+" / month ($"+total_cost+" total cost)");
        }
        else if(plan_type == 2 && duration > 0)
        {
            total_cost = duration * product_value * 12;
            total_cost = Number(total_cost).toFixed(2);
            installment_cost = total_cost / duration;
            installment_cost = Number(installment_cost).toFixed(2);
            $('.total-cost').text(" = $"+installment_cost+" / year ($"+total_cost+" total cost)");
        }
        else
        {
            
        }
    });

    $('#cust_form').click(function(){
        $('#submit_form').submit();
    });

    $('.aid-radio').click(function(){
        var selected_aid = $(this).val();
        if(selected_aid == "L")
        {
            $('.left-aid-form').show();
            $('.right-aid-form').hide();
        }
        else if(selected_aid == "R")
        {
            $('.left-aid-form').hide();
            $('.right-aid-form').show();
        }
    });


    // To get suffix for the days
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    if($('div').hasClass('billing-date'))
    {
        var billing_days_options = '';
        var days_count = new Date(new Date().getUTCFullYear(),new Date().getMonth()+1,0 ).getDate();
        var select_tag = "<select class='form-control select2 col-sm-2' name='recurring_date'>";
        for (i = 1; i <= days_count; i++) {
            if(new Date().getDate() == i)
                select_tag += "<option value='"+i+"' selected='selected'>"+getGetOrdinal(i)+"</option>"
            else
                select_tag += "<option value='"+i+"'>"+getGetOrdinal(i)+"</option>"
        }
        select_tag += "</select>"

        /* Append the Select tag to the form */
        $('.billing-date').append(select_tag);
        var plan_type = $("input[name='billing_frequency']:checked").val();

        if(plan_type == 1)
            duration = 'month';
        else if(plan_type == 2)
            duration = 'year';
        $('.repeatation').append(' <p class="billing-text"><b>'+monthNames[new Date().getMonth()]+'</b> every '+duration+'.</p>');
        
    }

    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"],
        v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
     }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.cancel-admin-btn').click(function(){
        var $this = $(this);
        var confirmed = confirm("CAUTION: Have you cancelled recurring transactions from Pay-Junction's Virtual Terminal for this customer ?");
        if(confirmed)
        {
            var customer_id = $this.attr('data-bind');

            var token;
            token = $('input[name=_token]').val();
            $.ajax({
                url: "deactivatecustomer",
                data: {customer_id : customer_id},
                type :'post',
//                headers: {'X-CSRF-TOKEN': token},
                success: function(data){
                    if(data.status)
                    {
                        $this.parents('tr:first').find('td:eq(2)').text('Inactive');
                        $this.fadeOut(2000);
                    }
            }});             
        }
     });

     $('.cancel-store-customer').click(function(){
        var $this = $(this);
        var confirmed = confirm("Are you sure you want to initiate cancellation request for the customer ?");
        if(confirmed)
        {
            var customer_id = $this.attr('data-bind');

            var token;
            token = $('input[name=_token]').val();
            $.ajax({
                url: "customercancelrequest",
                data: {customer_id : customer_id},
                type :'post',
//                headers: {'X-CSRF-TOKEN': token},
                success: function(data){
                    if(data.status)
                    {
                        $this.parents('tr:first').find('td:eq(2)').text('Cancellation Initiated');
                        $this.fadeOut(2000);
                    }
            }});             
        }
     });

     $('.select-store').change(function(){
         $('#hidden-store').val($('.select-store option:selected').text());
     });
});