<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <link rel="shortcut icon" href="{{ URL::asset('upload/'.getcong('site_favicon')) }}">
        <title>{{getcong('site_name')}}</title>
        <!--Morris Chart CSS -->
        <link href="{{ URL::asset('admin_assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ URL::asset('admin_assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ URL::asset('admin_assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ URL::asset('admin_assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('admin_assets/plugins/morris/morris.css') }}">
        <link href="{{ URL::asset('admin_assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
		<script src="{{ URL::asset('admin_assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/modernizr.min.js') }}"></script>
		<script> var APP_URL = {!! json_encode(url('/')) !!}; </script>
    </head>
    <body class="fixed-left">
       <!-- Begin page -->
        <div id="wrapper"> 
            <!-- Top Bar Start -->
                @include("admin.topbar")
            <!-- Top Bar End -->
            <!-- ========== Left Sidebar Start ========== -->
                @include("admin.sidebar");
            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
                <div class="content-page">
                     @yield("content")
                    <footer class="footer text-right">
                        &copy; {{date('Y')}}. All rights reserved.
                    </footer>
                </div>
              <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->    
		<script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <!--<script src="{{ URL::asset('admin_assets/js/jquery.min.js') }}"></script>-->
        <script src="{{ URL::asset('admin_assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/detect.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/fastclick.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/waves.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/wow.min.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/jquery.scrollTo.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/peity/jquery.peity.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/dataTables.bootstrap.js') }}"></script> 
		<script src="{{ URL::asset('admin_assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/jszip.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/pdfmake.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/vfs_fonts.js') }}"></script> 
		<script src="{{ URL::asset('admin_assets/plugins/datatables/buttons.html5.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/buttons.print.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/datatables/dataTables.colVis.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
        <!-- jQuery  -->
        <script src="{{ URL::asset('admin_assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/plugins/counterup/jquery.counterup.min.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/plugins/raphael/raphael-min.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/jquery-knob/jquery.knob.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
		<script src="{{ URL::asset('admin_assets/pages/datatables.init.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/js/jquery.core.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/pages/jquery.dashboard.js') }}"></script>
<!--        <script src="{{ URL::asset('admin_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>-->
        <script src="{{ URL::asset('admin_assets/js/jquery.app.js') }}"></script>
		<script src="{{ URL::asset('admin_assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
		<script>
		 jQuery(document).ready(function($) {
			jQuery('.counter').counterUp({
				delay: 100,
				time: 1200
			});

			jQuery(".knob").knob();
			jQuery('#user_datatable').dataTable();
			TableManageButtons.init();
			// Date Picker
			jQuery('#datepicker').datepicker();
			jQuery('#id_warranty_exp_left_aid').datepicker();
			jQuery('#id_warranty_exp_right_aid').datepicker();
		});
		</script>
@if(classActivePath('dashboard'))
<script type="text/javascript">
/**
* Theme: Ubold Admin Template
* Author: Coderthemes
* Morris Chart
*/
!function($) {
    "use strict";
    var Dashboard1 = function() {
        this.$realData = []
    };
    
    //creates Stacked chart
    Dashboard1.prototype.createStackedChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            stacked: true,
            labels: labels,
            hideHover: 'auto',
            resize: true, //defaulted to true
            gridLineColor: '#eeeeee',
            barColors: lineColors
        });
    },
 
    Dashboard1.prototype.init = function() {

        //creating Stacked chart
        var $stckedData  = [
            { m: 'Jan', a: {{$total_protection_plus_jan}}, b: {{$total_secure_jan}}, c: {{$total_secure_plus_jan}} },
            { m: 'Feb', a: {{$total_protection_plus_feb}}, b: {{$total_secure_feb}}, c: {{$total_secure_plus_feb}} },
            { m: 'Mar', a: {{$total_protection_plus_mar}}, b: {{$total_secure_mar}}, c: {{$total_secure_plus_mar}} },
            { m: 'Apr', a: {{$total_protection_plus_apr}}, b: {{$total_secure_apr}}, c: {{$total_secure_plus_apr}} },
            { m: 'May', a: {{$total_protection_plus_may}}, b: {{$total_secure_may}}, c: {{$total_secure_plus_may}} },
            { m: 'Jun', a: {{$total_protection_plus_jun}}, b: {{$total_secure_jun}}, c: {{$total_secure_plus_jun}} },
            { m: 'Jul', a: {{$total_protection_plus_jul}}, b: {{$total_secure_jul}}, c: {{$total_secure_plus_jul}} },
            { m: 'Aug', a: {{$total_protection_plus_aug}}, b: {{$total_secure_aug}}, c: {{$total_secure_plus_aug}} },
            { m: 'Sep', a: {{$total_protection_plus_sep}}, b: {{$total_secure_jan}}, c: {{$total_secure_plus_jan}} },
            { m: 'Oct', a: {{$total_protection_plus_oct}}, b: {{$total_secure_oct}}, c: {{$total_secure_plus_oct}} },
            { m: 'Nov', a: {{$total_protection_plus_nov}}, b: {{$total_secure_nov}}, c: {{$total_secure_plus_nov}} },
            { m: 'Dec', a: {{$total_protection_plus_dec}}, b: {{$total_secure_dec}}, c: {{$total_secure_plus_dec}} }
        ];
        this.createStackedChart('morris-bar-plans', $stckedData, 'm', ['a', 'b', 'c'], ['Protection+', 'Secure', 'Secure+'], ['#5fbeaa', '#5d9cec', '#ebeff2']);
    },
    //init
    $.Dashboard1 = new Dashboard1, $.Dashboard1.Constructor = Dashboard1
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Dashboard1.init();
}(window.jQuery);

</script>
@endif

    </body>
</html> 	 