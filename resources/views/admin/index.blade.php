<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="{{ URL::asset('upload/'.getcong('site_favicon')) }}">

        <title>{{getcong('site_name')}}</title>

        <link href="{{ URL::asset('admin_assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('admin_assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{{ URL::asset('admin_assets/js/modernizr.min.js') }}"></script>
        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class=" card-box">
            <div class="panel-heading"> 
                <h3 class="text-center"> Sign In to <strong class="text-custom">Miracle Ear</strong> </h3>
            </div> 
            <div class="message">
            <!--{!! Html::ul($errors->all(), array('class'=>'alert alert-danger errors')) !!}-->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif                
            </div>

            <div class="panel-body">
            
                {!! Form::open(array('url' => 'admin/login','class'=>'form-horizontal m-t-20','id'=>'loginform','role'=>'form')) !!}
                
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" name="email" placeholder="Email">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" placeholder="Password">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox" name="remember">
                            <label for="checkbox-signup">
                                Remember me
                            </label>
                        </div>
                        
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-inverse btn-block text-uppercase waves-effect waves-light" type="submit" style="background-color: #5fbeaa !important;
    border: 1px solid #5fbeaa !important;">Log In</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a href="{{ URL::to('admin/password/email') }}" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                    </div>
                </div>
            {!! Form::close() !!} 
            
            </div>   
            </div>                              
                 
            
        </div>
        
        

        
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ URL::asset('admin_assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/detect.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/fastclick.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/waves.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/wow.min.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/jquery.scrollTo.min.js') }}"></script>


        <script src="{{ URL::asset('admin_assets/js/jquery.core.js') }}"></script>
        <script src="{{ URL::asset('admin_assets/js/jquery.app.js') }}"></script>
    
    </body>
</html>