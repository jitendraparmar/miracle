@extends("admin.admin_app")

@section("content")

		<meta name="csrf-token" content="{{ csrf_token() }}" />
        <!-- Start content -->
        <div class="content">
            <div class="container">
				<!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Customers</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
                            </li>                            
                            <li class="active">
                                Customers
                            </li>
                        </ol>
                    </div
                </div>
				    <div class="row">
                        <div class="col-lg-12 col-md-12" style="z-index:99;">   

                                @if (count($errors) > 0)

                                <div class="alert alert-danger">

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <ul>

                                        @foreach ($errors->all() as $error)

                                            <li>{{ $error }}</li>

                                        @endforeach

                                    </ul>

                                </div>

                                @endif

                                @if(Session::has('flash_message'))

                                    <div class="alert alert-success">

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                        {{ Session::get('flash_message') }}

                                    </div>

                                @endif

                            </div>
                             
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <form role="form">
                                                <div class="form-group contact-search m-b-30">
                                                    <input type="text" id="search" class="form-control" placeholder="Search...">
                                                    <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                                </div> <!-- form-group -->
                                            </form>
                                        </div>
                                        <div class="col-sm-6">
                                             <a href="{{ URL::to('admin/customers/addcustomers') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" 
                                                                    data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> Add Customer</a>

                                              <div class="btn-group dropdown-btn-group pull-right">
                                                <a href="{{ URL::to('admin/customers/export_excel') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a">Excel</a>

                                                <a href="{{ URL::to('admin/customers/export_csv') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a">CSV</a>

                                                <a href="{{ URL::to('admin/customers/export_pdf') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a">PDF</a>
                                            </div>                           
                                        </div>
                                    </div>
                                    
                                    <div class="table-responsive">
                                        <table class="table table-hover mails m-0 table table-actions-bar">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="checkbox checkbox-primary checkbox-single m-r-15">
                                                            <input id="action-checkbox" type="checkbox">
                                                            <label for="action-checkbox"></label>
                                                        </div>
                                                         
                                                    </th>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                    <th>Email</th>                                                     
                                                    <th class="col-sm-2">Action</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                @foreach($allcustomers as $customers)
                                                <tr>
                                                    <td>
                                                        <div class="checkbox checkbox-primary m-r-15">
                                                            <input class="checkbox1" id="checkbox2" type="checkbox">
                                                            <label for="checkbox2"></label>
                                                        </div>
                                                        
                                                         
                                                    </td>
                                                    
                                                    <td>
                                                        {{$customers->first_name}} {{$customers->last_name}}
                                                    </td>
                                                    <td>
                                                          
                                                            @if($customers->customer_auth_status==1)
                                                                Active
                                                            @else
                                                                Inactive
                                                            @endif
                                                         
                                                    </td>
                                                    
                                                    <td>
                                                        {{$customers->email}}
                                                    </td>
                                                    
                                                     
                                                    <td>
                                                        <a href="{{ url('admin/customers/edit/'.$customers->id) }}" class="table-action-btn" title="Edit"><i class="md md-edit"></i></a>
                                                        <a href="{{ url('admin/customers/delete/'.$customers->id) }}" class="table-action-btn" title="Delete"><i class="md md-close"></i></a>
															@if(Auth::User()->usertype == "Admin")
																@if($customers->customer_auth_status==1)
																	<a href="javascript:void(0);" class="table-action-btn cancel-admin-btn" data-bind="{{$customers->id}}" title="Deactivate"><i class="md md-remove-circle"></i></a>
																@endif
															@else
																@if($customers->customer_auth_status==1)
																	<a href="javascript:void(0);" class="table-action-btn cancel-store-customer" data-bind="{{$customers->id}}" title="Deactivate"><i class="md md-remove-circle"></i></a>
																@endif
															@endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                                
                                                 
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                            </div> <!-- end col -->

                            
                        </div>

                    <script type="text/javascript">
                    $(document).ready(function() {
                    $('#action-checkbox').click(function(event) {  //on click 
                        if(this.checked) { // check select status
                            $('.checkbox1').each(function() { //loop through each checkbox
                                this.checked = true;  //select all checkboxes with class "checkbox1"               
                            });
                        }else{
                            $('.checkbox1').each(function() { //loop through each checkbox
                                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                            });         
                        }
                    });
                    
                });
                    </script>
             </div>
         </div>    

<script src="{{ URL::asset('admin_assets/js/customer.js') }}"></script>
@endsection
