@extends("admin.admin_app")



@section("content")

        

        <!-- Start content -->

        <div class="content">

            <div class="container">

				<!-- Page-Title -->

                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="page-title">{{ isset($customer->id) ? 'Edit Customer' : 'Add Customer' }}</h4>

                        <ol class="breadcrumb">

                            <li>

                                <a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>

                            </li>

                             <li>

                                <a href="{{ URL::to('admin/customers') }}">Customers</a>

                            </li>                            

                            <li class="active">

                                {{ isset($customer->id) ? 'Edit Customer' : 'Add Customer' }}

                            </li>

                        </ol>

                    </div>

                </div>

				    <div class="row">

                            <div class="col-lg-12 col-md-12" style="z-index:99;">   

                                @if (count($errors) > 0)

                                <div class="alert alert-danger">

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <ul>

                                        @foreach ($errors->all() as $error)

                                            <li>{{ $error }}</li>

                                        @endforeach

                                    </ul>

                                </div>

                                @endif

                                @if(Session::has('flash_message'))

                                    <div class="alert alert-success">

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                        {{ Session::get('flash_message') }}

                                    </div>

                                @endif

                            </div>

                            <div class="col-lg-12">

                              
                                    {!! Form::open(array('url' => array('admin/customers/addcustomers'),'class'=>'','name'=>'customers_form','id'=>'customers_form','role'=>'form','enctype' => 'multipart/form-data')) !!}

                                         <input type="hidden" name="id" value="{{ isset($customer->id) ? $customer->id : null }}">



                                          <div class="card-box">

                                            <div class="row">

                                                    <h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>PERSONAL</b>        @if(isset($customer->id))   
                                                    
                                                <span class="account_id" style="display:inline;">Account ID:  {!! $account_id!!}</span>
                                                
                                                
                                                @endif</h5>
                                                    
                                             

                                                    <div class="row">

                                                    <div class="col-sm-6">

                                                    <div class="form-group m-b-20">

                                                        <label>Customers Authorization

Status <span class="text-danger">*</span></label>

                                                        <select class="form-control select2" name="customer_auth_status">

                                                            <option value="1" @if(isset($customer->customer_auth_status) and  $customer->customer_auth_status=='1') selected @endif >Active</option>

                                                            <option value="0" @if(isset($customer->customer_auth_status) and  $customer->customer_auth_status=='0') selected @endif>Canceled</option>

                                                             

                                                             

                                                        </select>    

                                                    </div>

                                                    </div><br/>

                                                    <div class="col-sm-6">

                                                    <div class="form-group m-b-20">

                                                        <label>Gender <span class="text-danger">*</span></label><br/>

                                                        <div class="radio radio-inline">

                                                                <input type="radio" id="inlineRadio4" value="male" name="gender" @if(isset($customer->gender) and  $customer->gender=='male') checked @endif>

                                                                <label for="inlineRadio4"> Male </label>

                                                            </div>

                                                            <div class="radio radio-inline">

                                                                <input type="radio" id="inlineRadio5" value="female" name="gender" @if(isset($customer->gender) and  $customer->gender=='female') checked @endif>

                                                                <label for="inlineRadio5"> Female </label>

                                                            </div>

                                                    </div>

                                                    </div>

                                                </div>

                                                    <div class="row">

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>First Name <span class="text-danger">*</span></label>

                                                            <input type="text" name="first_name" class="form-control" placeholder="" value="{{ isset($customer->first_name) ? $customer->first_name : null }}">

                                                        </div>

                                                        </div>

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>Last Name <span class="text-danger">*</span></label>

                                                            <input type="text" name="last_name" class="form-control" placeholder="" value="{{ isset($customer->last_name) ? $customer->last_name : null }}">

                                                        </div>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>Date of Birth</label>         

                                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" name="birth_date" id="datepicker" value="{{ isset($customer->birth_date) ? date('m/d/Y',$customer->birth_date) : null }}">

                                                                    

                                                        </div>

                                                        </div>

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>E-mail Address <span class="text-danger">*</span></label>

                                                            <input type="text" name="email" class="form-control" placeholder="" value="{{ isset($customer->email) ? $customer->email : null }}">

                                                        </div>

                                                        </div>

                                                     </div>   





                                                    <h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>Shipping Address</b></h5> 

                                                    <div class="row">                       

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>Street Address <span class="text-danger">*</span></label>

                                                            <input type="text" name="address" class="form-control" placeholder="" value="{{ isset($customer->address) ? $customer->address : null }}">

                                                        </div>

                                                        </div>

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>City <span class="text-danger">*</span></label>

                                                            <input type="text" name="city" class="form-control" placeholder="" value="{{ isset($customer->city) ? $customer->city : null }}">



                                                        </div>

                                                        </div>

                                                    </div>

                                                    <div class="row">

                                                    <div class="col-sm-6">

                                                    <div class="form-group m-b-20">

                                                        <label>State <span class="text-danger">*</span></label>

                                                         <select name="state" class="selectpicker" data-live-search="true"  data-style="btn-white">

                                                            <option value="">Select One</option>    

                                                            @foreach($allstate as $state)

                                                            <option value="{{$state->id}}" @if(isset($customer->state) && $state->id==$customer->state) selected @endif>{{$state->state_name}}</option>

                                                            @endforeach

                                                            

                                                        </select>



                                                    </div>

                                                    </div>

                                                    <div class="col-sm-6">

                                                    <div class="form-group m-b-20">

                                                        <label>Post Code <span class="text-danger">*</span></label>

                                                        <input type="text" name="post_code" class="form-control" placeholder="" value="{{ isset($customer->post_code) ? $customer->post_code : null }}">



                                                    </div>

                                                    </div>

                                                    </div>

                                                    <div class="row">

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>Country</label>

                                                            <input type="text" name="country" class="form-control" placeholder="" value="{{ isset($customer->country) ? $customer->country : 'United States' }}" readonly>



                                                        </div>

                                                        </div>

                                                   </div>



                                                    <h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>CONTACT</b></h5>

                                                    <div class="row">

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>Telephone Number <span class="text-danger">*</span></label>

                                                            <input type="text" name="telephone_number" class="form-control" placeholder="" value="{{ isset($customer->telephone_number) ? $customer->telephone_number : null }}">



                                                        </div>

                                                        </div>

                                                    </div>

                                                    <h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>PRODUCT SELECTION</b></h5>

                                                    <div class="row">

                                                        <div class="col-sm-10">

                                                    <div class="form-group">                    

                                                  <div class="radio radio-inline radio-single col-md-7">

                                                    <input type="radio" aria-label="Single radio Two" name="product_selection" id="radio1" value="14.95" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='14.95') checked @endif>

                                                    <label for="radio1">Protection+ Supplemental Loss & Damage Monthly Option</label>

                                                  </div>

                                                  <div class="col-md-3">

                                                    <p class="text-left">$14.95</p>

                                                  </div>

                                                </div>

                                                <div class="form-group">                    

                                                  <div class="radio radio-inline radio-single col-md-7">

                                                    <input type="radio" aria-label="Single radio Two" name="product_selection" id="radio2" value="179.40" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='179.40') checked @endif>

                                                    <label for="radio2">Protection+ Supplemental Loss & Damage Yearly Option</label>

                                                  </div>

                                                  <div class="col-md-3">

                                                    <p class="text-left">$179.40</p>

                                                  </div>

                                                </div>

                                                <div class="form-group">                    

                                                  <div class="radio radio-inline radio-single col-md-7">

                                                    <input type="radio" aria-label="Single radio Two" name="product_selection" id="radio3" value="21.95" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='21.95') checked @endif>

                                                    <label for="radio3">Secure Plan Supplemental Repair Warranty Monthly Option </label>

                                                  </div>

                                                  <div class="col-md-3">

                                                    <p class="text-left">$21.95</p>

                                                  </div>

                                                </div>

                                                <div class="form-group">                    

                                                  <div class="radio radio-inline radio-single col-md-7">

                                                    <input type="radio" aria-label="Single radio Two" name="product_selection" id="radio4" value="14.45" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='14.45') checked @endif>

                                                    <label for="radio4">SINGLE AID-Secure Supplemental Repair Warranty Monthly Option</label>

                                                  </div>

                                                  <div class="col-md-3">

                                                    <p class="text-left">$14.45</p>

                                                  </div>

                                                </div>

                                                <div class="form-group">                    

                                                  <div class="radio radio-inline radio-single col-md-7">

                                                    <input type="radio" aria-label="Single radio Two" name="product_selection" id="radio5" value="263.40" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='263.40') checked @endif>

                                                    <label for="radio5">Secure Plan Supplemental Repair Warranty Yearly Option </label>

                                                  </div>

                                                  <div class="col-md-3">

                                                    <p class="text-left">$263.40</p>

                                                  </div>

                                                </div>

                                                <div class="form-group">                    

                                                  <div class="radio radio-inline radio-single col-md-7">

                                                    <input type="radio" aria-label="Single radio Two" name="product_selection" id="radio6" value="173.40" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='173.40') checked @endif>

                                                    <label for="radio6">SINGLE AID-Secure Supplemental Repair Warranty Yearly Option</label>

                                                  </div>

                                                  <div class="col-md-3">

                                                    <p class="text-left">$173.40</p>

                                                  </div>

                                                </div>

                                                <div class="form-group">                    

                                                  <div class="radio radio-inline radio-single col-md-7">

                                                    <input type="radio" aria-label="Single radio Two" name="product_selection" id="radio7" value="599.40" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='599.40') checked @endif>

                                                    <label for="radio7">Secure+ Plan Supplemental Repair Warranty Yearly Option </label>

                                                  </div>

                                                  <div class="col-md-3">

                                                    <p class="text-left">$599.40</p>

                                                  </div>

                                                </div>

                                                <div class="form-group">                    

                                                  <div class="radio radio-inline radio-single col-md-7">

                                                    <input type="radio" aria-label="Single radio Two" name="product_selection" id="radio8" value="369.40" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='369.40') checked @endif>

                                                    <label for="radio8">SINGLE AID-Secure+ Plan Supplemental Repair Warranty Yearly Option </label>

                                                  </div>

                                                  <div class="col-md-3">

                                                    <p class="text-left">$369.40</p>

                                                  </div>

                                                </div>

                                                </div>

                                            </div>

                                            <h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>Hearing Aid Information</b></h5>



                                            <div class="row">

                                                <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>Store <span class="text-danger">*</span></label>

                                                            @if(Auth::user()->usertype=='Admin')



                                                                <select name="store_number" class="selectpicker" data-live-search="true"  data-style="btn-white">

                                                                    <option value="">Select One</option>    

                                                                    @foreach($locations as $location)

                                                                    <option value="{{$location->store_number}}" @if(isset($customer->store_number) && $location->store_number==$customer->store_number) selected @endif>{{$location->store_name}}</option>

                                                                    @endforeach

                                                                

                                                                </select>



                                                            @else



                                                            <?php

                                                                $location_id=Auth::User()->location_id;?>

                                                             {{ \App\Location::getStoreInfo($location_id)->store_number}}



                                                             <input type="hidden" name="store_number" class="form-control" placeholder="" value="{{ isset($customer->store_number) ? $customer->store_number : \App\Location::getStoreInfo($location_id)->store_number }}">



                                                             @endif



                                                              

                                                        </div>

                                                        </div>

                                            </div>

                                            <div class="row">

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>Hearing Specialist <span class="text-danger">*</span></label>

                                                            <input type="text" name="hearing_specialist_name" class="form-control" placeholder="" value="{{ isset($customer->hearing_specialist_name) ? $customer->hearing_specialist_name : null }}">

                                                        </div>

                                                        </div>

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">

                                                            <label>Sycle ID # </label>

                                                            <input type="text" name="sycle_id" class="form-control" placeholder="" value="{{ isset($customer->sycle_id) ? $customer->sycle_id : null }}">

                                                        </div>

                                                        </div>

                                             </div>

                                             <div class="row">

                                                <div class="col-sm-12"> 

                                                    

                                                    <div class="col-sm-6">

                                                    <h5 class="m-t-30"><b>Left Hearing Aid</b></h5>    

                                                    <div class="form-group m-b-20">

                                                        <label>Warranty Expiration </label>

                                                        <input type="text" name="id_warranty_exp_left_aid" id="id_warranty_exp_left_aid" class="form-control" placeholder="mm/dd/yyyy" value="{{ isset($customer->id_warranty_exp_left_aid) ? date('m/d/Y',$customer->id_warranty_exp_left_aid) : null }}">

                                                    </div>

                                                    <div class="form-group m-b-20">

                                                        <label>Serial # </label>

                                                        <input type="text" name="serial_number_left_aid" class="form-control" placeholder="" value="{{ isset($customer->serial_number_left_aid) ? $customer->serial_number_left_aid : null }}">

                                                    </div>

                                                    <div class="form-group m-b-20">

                                                        <label>Battery Size</label>

                                                        <select class="form-control select2" name="battery_size_left_aid">

                                                        <option value="0">Select One...</option>

                                                        <option value="10" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='10') selected @endif>10</option>

                                                        <option value="13" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='13') selected @endif>13</option>

                                                        <option value="312" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='312') selected @endif>312</option>

                                                        <option value="675" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='675') selected @endif>675</option>

                                                        <option value="R_10"@if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='R_10') selected @endif>Rechargeable 10</option>

                                                        <option value="R_13" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='R_13') selected @endif>Rechargeable 13</option>

                                                        <option value="R_312" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='R_312') selected @endif>Rechargeable 312</option>

                                                        <option value="R_675" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='R_675') selected @endif>Rechargeable 675</option>

                                                        </select>

                                                    </div>



                                                        

                                                    </div>

                                                    <div class="col-sm-6">

                                                    <h5 class="m-t-30"><b>Right Hearing Aid</b></h5>    

                                                    <div class="form-group m-b-20">

                                                        <label>Warranty Expiration </label>

                                                        <input type="text" name="id_warranty_exp_right_aid" id="id_warranty_exp_right_aid" class="form-control" placeholder="mm/dd/yyyy" value="{{ isset($customer->id_warranty_exp_right_aid) ? date('m/d/Y',$customer->id_warranty_exp_right_aid) : null }}">

                                                    </div>

                                                    <div class="form-group m-b-20">

                                                        <label>Serial # </label>

                                                        <input type="text" name="serial_number_right_aid" class="form-control" placeholder="" value="{{ isset($customer->serial_number_right_aid) ? $customer->serial_number_right_aid : null }}">

                                                    </div>

                                                    <div class="form-group m-b-20">

                                                        <label>Battery Size</label>

                                                        <select class="form-control select2" name="battery_size_right_aid">

                                                        <option value="0">Select One...</option>

                                                        <option value="10" @if(isset($customer->battery_size_right_aid) and  $customer->battery_size_right_aid=='10') selected @endif>10</option>

                                                        <option value="13" @if(isset($customer->battery_size_right_aid) and  $customer->battery_size_right_aid=='13') selected @endif>13</option>

                                                        <option value="312" @if(isset($customer->battery_size_right_aid) and  $customer->battery_size_right_aid=='312') selected @endif>312</option>

                                                        <option value="675" @if(isset($customer->battery_size_right_aid) and  $customer->battery_size_right_aid=='675') selected @endif>675</option>

                                                        <option value="R_10" @if(isset($customer->battery_size_right_aid) and  $customer->battery_size_right_aid=='R_10') selected @endif>Rechargeable 10</option>

                                                        <option value="R_13" @if(isset($customer->battery_size_right_aid) and  $customer->battery_size_right_aid=='R_13') selected @endif>Rechargeable 13</option>

                                                        <option value="R_312" @if(isset($customer->battery_size_right_aid) and  $customer->battery_size_right_aid=='R_312') selected @endif>Rechargeable 312</option>

                                                        <option value="R_675" @if(isset($customer->battery_size_right_aid) and  $customer->battery_size_right_aid=='R_675') selected @endif>Rechargeable 675</option>

                                                        </select>

                                                    </div>

                                                    </div>

                                                

                                                </div>

                                               </div>         

                                            <div class="row">

                                                        <div class="col-sm-6">

                                                        <div class="form-group m-b-20">
                                                          <label>Monthly Reoccuring</label>
                                                        
                                                              <select class="form-control select2" name="monthly_reoccuring">

                                                            

                                                                <option value="0">Select</option>



                                                                @for($n=1; $n<=28;$n++)

                                                                <option value="{{$n}}" @if(isset($customer->monthly_reoccuring) and  $customer->monthly_reoccuring==$n) selected @endif>{{$n}}</option>

                                                                @endfor

                                                            </select>
                                                            
                                                            </div>
                                                            </div>
<div class="col-sm-6">
                                                            <label>Billing Frequency</label>

                                                             <select class="form-control select2" name="billing_frequency">

                                                            

                                                                <option value="0">Select</option>




                                                                <option value="1" @if(isset($billing_frequency) and  $billing_frequency==1) selected @endif>Monthly</option>


<option value="3" @if(isset($billing_frequency) and  $billing_frequency==3) selected @endif>Quarterly (3 Months)</option>

<option value="12" @if(isset($billing_frequency) and  $billing_frequency==12) selected @endif>Annually</option>

<option value="12" @if(isset($billing_frequency) and  $billing_frequency==12) selected @endif>Daily</option>
                                                            </select>

</div>
                                                        </div>


                                          



                                            <div class="col-sm-12">

                                                <div class="form-group ">                                                      

                                                <button type="submit" class="btn btn-default waves-effect waves-light m-l-5">

                                                    Submit

                                                </button>

                                                  

                                                </div>

                                            </div>    



                                                      

                                        </div>

                                    </div>



                                {!! Form::close() !!}


 </div>
                         
 </div>
     
@if(isset($customer->id))   

<div class="card-box">
 {!! Form::open(array('url' => array('admin/customers/processpayments'),'class'=>'','name'=>'customer_payment_form','id'=>'customer_payment_form','role'=>'form','enctype' => 'multipart/form-data')) !!}
<h5 class="text-muted text-uppercase m-t-0 m-b-20" style="display: inline;margin-right: 50px;"
><b>PAYMENT INFORMATION</b></h5> <div style="display:inline;"> {!! $processMembership !!}</div>

<br /><br />

  <input type="hidden" name="id" value="{{ isset($customer->id) ? $customer->id : null }}">
    <input type="hidden" name="type" value="charge">

      <div class="row">
        <div class="col-md-4">
        
         <div class="form-group">
         {!! Form::label(null, 'Credit card number:') !!}
          {!! Form::text('cardNumber', null, ['class' => 'form-control']) !!}
          
           </div>
      
       
        </div>
        <div class="col-md-4">
            <div class="form-group">
              {!! Form::label(null, 'Ex. Month') !!}
              
              {{ Form::selectMonth('cardExpMonth', 7, ['class' => 'form-control']) }}
              
                   

              
          </div> 
       </div>
       
        <div class="col-md-4">
            <div class="form-group">
              {!! Form::label(null, 'Ex. Year') !!}
              {!! Form::selectYear('cardExpYear', date('Y'), date('Y') + 10, null, ['class' => 'form-control']) !!}
          </div>
        </div>
        </div>
        
        
             <div class="form-group">
      <!--  {-- Form::select('type', array('charge' => 'Charge & Update', 'update' => 'Update Only')); --} -->
  </div>
  <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-order', 'id' => 'ccsubmitBtn', 'style' => 'margin-bottom: 10px;']) !!}
        </div>
      
      </div>

      
                                
              </div>                  
                       

{!! Form::close() !!}
                            </div> <!-- end col -->
  @endif

@if(isset($customer->id))   

<div class="card-box">
 {!! Form::open(array('url' => array('admin/customers/achprocesspayments'),'class'=>'','name'=>'customer_payment_form','id'=>'customer_payment_form','role'=>'form','enctype' => 'multipart/form-data')) !!}
<h5 class="text-muted text-uppercase m-t-0 m-b-20" style="display: inline;margin-right: 50px;"
><b>ACH PAYMENT INFORMATION</b></h5> <div style="display:inline;"> {!! $processMembership !!}</div>

<br /><br />

  <input type="hidden" name="id" value="{{ isset($customer->id) ? $customer->id : null }}">
    <input type="hidden" name="type" value="charge">

      <div class="row">
        <div class="col-md-4">
        
         <div class="form-group">
         {!! Form::label(null, 'Routing Number:') !!}
          {!! Form::text('routingNumber', null, ['class' => 'form-control']) !!}
          
           </div>
      
       
        </div>
        <div class="col-md-4">
            <div class="form-group">
              {!! Form::label(null, 'Account Number') !!}
              
              {!! Form::text('accNumber', null, ['class' => 'form-control']) !!}
                          
          </div> 
       </div>
       
        <!--div class="col-md-4">
            <div class="form-group">
              {!! Form::label(null, 'Account Type') !!}
               {!! Form::text('accType', null, ['class' => 'form-control']) !!}
          </div>
        </div-->

        </div>
        
        
             <div class="form-group">
      <!--  {-- Form::select('type', array('charge' => 'Charge & Update', 'update' => 'Update Only')); --} -->
  </div>
  <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-order', 'id' => 'ccsubmitBtn', 'style' => 'margin-bottom: 10px;']) !!}
        </div>
      
      </div>

      
                                
              </div>                  
                       

{!! Form::close() !!}
                            </div> <!-- end col -->
  @endif

                            

                        </div>



             

             </div>

         </div>    



@endsection
