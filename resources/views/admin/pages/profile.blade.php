@extends("admin.admin_app")

@section("content")

  <!-- Start content -->
                <div class="content">

                    <div class="wraper container">

                        <!-- Page-Title -->
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="page-title">Profile</h4>
                            <ol class="breadcrumb">
                                <li><a href="{{ URL::to('admin/dashboard') }}">Dashboard</a></li>                                 
                                <li class="active">Profile</li>
                            </ol>
                        </div>
                    </div>

                        <div class="row">
                           <div class="col-lg-12 col-md-12">   
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if(Session::has('flash_message'))
                                    <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ Session::get('flash_message') }}
                                    </div>
                                @endif
                            </div>            
                            <div class="col-lg-12 col-md-12">
                                
                                <ul class="nav nav-tabs tabs">
                                    <li class="active tab">
                                        <a href="#profile" data-toggle="tab" aria-expanded="false"> 
                                            <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                            <span class="hidden-xs">Profile</span> 
                                        </a> 
                                    </li> 
                                    <li class="tab"> 
                                        <a href="#password" data-toggle="tab" aria-expanded="false"> 
                                            <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                            <span class="hidden-xs">Password</span> 
                                        </a> 
                                    </li> 
                                      
                                    
                                </ul> 
                                <div class="tab-content"> 
                                    <div class="tab-pane active" id="profile"> 
                                        {!! Form::open(array('url' => 'admin/profile','class'=>'form-horizontal','name'=>'account_form','id'=>'account_form','role'=>'form','enctype' => 'multipart/form-data')) !!}
                                        

                                        <div class="form-group">
                                            <label for="avatar" class="col-sm-3 control-label">Profile Picture</label>
                                            <div class="col-sm-9">
                                                <div class="media">
                                                    <div class="media-left">
                                                        @if(Auth::user()->image_icon)
                                                         
                                                            <img src="{{URL::to(Auth::user()->image_icon)}}" width="80" alt="person">
                                                        
                                                        @else
                                                        
                                                            <img src="{{ URL::asset('admin_assets/images/guy.jpg') }}" alt="person" class="img-circle" width="80"/>
                                                    
                                                        @endif
                                                         
                                                                                        
                                                    </div>
                                                    <div class="media-body media-middle">
                                                        <input type="file" name="user_icon" class="filestyle">
                                                        <small class="text-muted bold">Size 200x200px</small>
                                                    </div>
                                                </div>
                            
                                            </div>
                                        </div>
                                         
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">First Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="first_name" value="{{ Auth::user()->first_name }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Last Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="last_name" value="{{ Auth::user()->last_name }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="email" name="email" value="{{ Auth::user()->email }}" class="form-control" value="">
                                            </div>
                                        </div>                                       
                                          
                                        <hr>
                                          <div class="form-group">
                                            <div class="col-md-offset-3 col-sm-9 ">
                                                <button type="submit" class="btn btn-default">Save Changes </button>
                                            </div>
                                        </div>
                                                
                                                 
                                            {!! Form::close() !!} 
                                    </div> 
                                    <div class="tab-pane" id="password">
                                       {!! Form::open(array('url' => 'admin/profile_pass','class'=>'form-horizontal','name'=>'pass_form','id'=>'pass_form','role'=>'form')) !!}
                
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">New Password</label>
                                            <div class="col-sm-9">
                                                <input type="password" name="password" value="" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Confirm Password</label>
                                            <div class="col-sm-9">
                                                <input type="password" name="password_confirmation" value="" class="form-control" value="">
                                            </div>
                                        </div>
                                         
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-md-offset-3 col-sm-9 ">
                                                <button type="submit" class="btn btn-default">Save Changes </button>
                                            </div>
                                        </div>

                                    {!! Form::close() !!} 
                                    </div> 
                                     
                                     
                                </div> 


                                
                            </div>

                        </div>



                    </div> <!-- container -->
                               
                </div> <!-- content -->

@endsection