@extends("admin.admin_app")
@section("content")
<!-- Start content -->
<div class="content">
	<div class="container">
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Dashboard</h4>
				<p class="text-muted page-title-alt">Welcome to Your Protection Plan Admin Panel!</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-3">
				<div class="widget-bg-color-icon card-box fadeInDown animated">
					<div class="bg-icon bg-icon-info pull-left">
						<i class="md md-attach-money text-info"></i>
					</div>
					<div class="text-right">
						<h3 class="text-dark"><b class="counter">{{$all_revanue}}</b></h3>
						<p class="text-muted"><a href="{{ URL::to('admin/breakdown') }}">MTD Revenue</a></p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="widget-bg-color-icon card-box">
					<div class="bg-icon bg-icon-pink pull-left">
						<i class="md md-add-shopping-cart text-pink"></i>
					</div>
					<div class="text-right">
						<h3 class="text-dark"><b class="counter">{{$today_customers}}</b></h3>
						<p class="text-muted">Today's Sales</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="widget-bg-color-icon card-box">
					<div class="bg-icon bg-icon-purple pull-left">
						<i class="md md-equalizer text-purple"></i>
					</div>
					<div class="text-right">
						<h3 class="text-dark"><b class="counter">{{$active_customers}}</b></h3>
						<p class="text-muted">Active Customers</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="widget-bg-color-icon card-box">
					<div class="bg-icon bg-icon-success pull-left">
						<i class="md md-remove-red-eye text-success"></i>
					</div>
					<div class="text-right">
						<h3 class="text-dark"><b class="counter">{{$new_customers}}</b></h3>
						<p class="text-muted">New Customers</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="card-box">
					<h4 class="text-dark header-title m-t-0 m-b-30">Active Plans</h4>
					<div class="widget-chart text-center">
						<input class="knob" data-width="150" data-height="150" data-linecap="round" data-fgColor="#fb6d9d" value="{{$active_plans}}" data-max="{{$active_plans}}" data-skin="tron" data-displayinput="true" data-readOnly="true" data-thickness=".15"/>
						<h5 class="text-muted m-t-20">Total plans made this month</h5>
						<h2 class="font-600">{{$this_month_plans}}</h2>
						<ul class="list-inline m-t-15">
							<li>
								<h5 class="text-muted m-t-20">Protection+</h5>
								<h4 class="m-b-0">{{$total_protection_plus}}</h4>
							</li>
							<li>
								<h5 class="text-muted m-t-20">Secure</h5>
								<h4 class="m-b-0">{{$total_secure_plan}}</h4>
							</li>
							<li>
								<h5 class="text-muted m-t-20">Secure+</h5>
								<h4 class="m-b-0">{{$total_secure_plus_plan}}</h4>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="card-box">
					<h4 class="text-dark header-title m-t-0">Plans sold by month</h4>
					<div class="text-center">
						<ul class="list-inline chart-detail-list">
							<li>
								<h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa;"></i>Protection+</h5>
							</li>
							<li>
								<h5><i class="fa fa-circle m-r-5" style="color: #5d9cec;"></i>Secure</h5>
							</li>
							<li>
								<h5><i class="fa fa-circle m-r-5" style="color: #dcdcdc;"></i>Secure+</h5>
							</li>
						</ul>
					</div>
					<div id="morris-bar-plans" style="height: 303px;"></div>
				</div>
			</div>
		</div>
		<!-- end row -->
	</div> <!-- container -->
</div> <!-- content -->
@endsection