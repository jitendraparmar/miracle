@extends("admin.admin_app")

@section("content")
        
        <!-- Start content -->
        <div class="content">
            <div class="container">
				<!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Store Admin</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
                            </li>                            
                            <li class="active">
                                Store Admin
                            </li>
                        </ol>
                    </div>
                </div>
				  <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                              
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="m-b-30">
                                            
                                            <a class="pull-right btn btn-default waves-effect waves-light" href="{{URL::to('admin/users/adduser')}}"><i class="fa fa-plus"></i> Add Admin</a>
                                        </div>
                                    </div>
                                </div>
                             <p>&nbsp;</p>

                            @if(Session::has('flash_message'))
                                <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('flash_message') }}
                                </div>
                            @endif
                             <p>&nbsp;</p>
                             

                            <table id="user_datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Store</th>
                                    <th class="text-center" style="width: 10%;">Actions</th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach($allusers as $i => $users)
                                    <tr>
                                        <td>{{ $users->first_name }}</td>
                                        <td>{{ $users->last_name }}</td>
                                        <td>{{ $users->email}}</td>
                                        <td>{{ \App\Location::getStoreInfo($users->location_id)->store_name}}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ url('admin/users/adduser/'.$users->id) }}" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
                                                
                                                <a href="{{ url('admin/users/delete/'.$users->id) }}" class="on-default remove-row" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>

                                                
                                            </div>
                                        
                                    </td>
                                        
                                    </tr>
                                   @endforeach
                                
                                
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>

             
             </div>
         </div>    

@endsection