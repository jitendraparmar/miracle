@extends("admin.admin_app")

@section("content")

                <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">{{ isset($location->id) ? 'Edit Location' : 'Add Location' }}</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ URL::to('admin/locations') }}">Locations</a>
                            </li>                            
                            <li class="active">
                                {{ isset($location->id) ? 'Edit Location' : 'Add Location' }}
                            </li>
                        </ol>
                    </div>
                </div>
                  <div class="row">
                           <div class="col-lg-12 col-md-12">   
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if(Session::has('flash_message'))
                                    <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ Session::get('flash_message') }}
                                    </div>
                                @endif
                            </div>            
                            <div class="col-lg-12 col-md-12">
                                
                                 
                                <div class="card-box"> 
                                      
                                   {!! Form::open(array('url' => array('admin/locations/addlocation'),'class'=>'form-horizontal','name'=>'location_form','id'=>'location_form','role'=>'form','enctype' => 'multipart/form-data')) !!} 
                                    <input type="hidden" name="id" value="{{ isset($location->id) ? $location->id : null }}">
                
                
                                       <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Store Name <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="store_name" value="{{ isset($location->store_name) ? $location->store_name : null }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Store Number <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="store_number" value="{{ isset($location->store_number) ? $location->store_number : null }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Store Address <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="store_address" value="{{ isset($location->store_address) ? $location->store_address : null }}" class="form-control">
                    </div>
                </div>                
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Store City <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="store_city" value="{{ isset($location->store_city) ? $location->store_city : null }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Store State <span class="text-danger">*</span></label>
                    <div class="col-sm-9">                         
                        <select name="store_state" class="selectpicker" data-live-search="true"  data-style="btn-white">
                            <option value="">Select One</option>    
                            @foreach($allstate as $state)
                            <option value="{{$state->id}}" @if(isset($location->store_state) && $state->id==$location->store_state) selected @endif>{{$state->state_name}}</option>
                            @endforeach
                            
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Store Zip <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="store_zip" value="{{ isset($location->store_zip) ? $location->store_zip : null }}" class="form-control">
                    </div>
                </div>
               
                               
                <hr>
                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-9 ">
                        <button type="submit" class="btn btn-default">{{ isset($location->id) ? 'Edit Location' : 'Add Location' }}</button>
                         
                    </div>
                </div>
                
                {!! Form::close() !!}
                                     
                                </div> 


                                
                            </div>

                        </div>

             
             </div>
         </div>      

@endsection