@extends("admin.admin_app")

@section("content")

                <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">{{ isset($user->id) ? 'Edit Admin' : 'Add Admin' }}</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ URL::to('admin/users') }}">Store Admin</a>
                            </li>                            
                            <li class="active">
                                {{ isset($user->id) ? 'Edit Admin' : 'Add Admin' }}
                            </li>
                        </ol>
                    </div>
                </div>
                  <div class="row">
                           <div class="col-lg-12 col-md-12">   
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if(Session::has('flash_message'))
                                    <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ Session::get('flash_message') }}
                                    </div>
                                @endif
                            </div>            
                            <div class="col-lg-12 col-md-12">
                                
                                 
                                <div class="card-box"> 
                                      
                                   {!! Form::open(array('url' => array('admin/users/adduser'),'class'=>'form-horizontal','name'=>'user_form','id'=>'user_form','role'=>'form','enctype' => 'multipart/form-data')) !!} 
                                    <input type="hidden" name="id" value="{{ isset($user->id) ? $user->id : null }}">
                
                
                                       <div class="form-group">
                    <label for="" class="col-sm-3 control-label">First Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="first_name" value="{{ isset($user->first_name) ? $user->first_name : null }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="last_name" value="{{ isset($user->last_name) ? $user->last_name : null }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="text" name="email" value="{{ isset($user->email) ? $user->email : null }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" name="password" value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Location</label>
                    <div class="col-sm-9">
                        <select name="location_id" class="selectpicker" data-live-search="true"  data-style="btn-white">
                            <option value="">Select One</option>    
                            @foreach($locations as $location)
                            <option value="{{$location->id}}" @if(isset($user->location_id) && $location->id==$user->location_id) selected @endif>{{$location->store_name}}</option>
                            @endforeach
                            
                        </select>
                    </div>
                </div>
                
                <hr>
                 
                <div class="form-group">
                    <label for="avatar" class="col-sm-3 control-label">Profile Picture</label>
                    <div class="col-sm-9">
                        <div class="media">
                            <div class="media-left">
                                @if(isset($user->image_icon))
                                 
                                    <img src="{{URL::to($user->image_icon)}}" width="100" alt="person">
                                @endif
                                                                
                            </div>
                            <div class="media-body media-middle">
                                <input type="file" name="image_icon" class="filestyle"> 
                            </div>
                        </div>
    
                    </div>
                </div>                  
                <hr>
                <div class="form-group">
                    <div class="col-md-offset-3 col-sm-9 ">
                        <button type="submit" class="btn btn-default">{{ isset($user->id) ? 'Edit Admin' : 'Add Admin' }}</button>
                         
                    </div>
                </div>
                
                {!! Form::close() !!}
                                     
                                </div> 


                                
                            </div>

                        </div>

             
             </div>
         </div>      

@endsection