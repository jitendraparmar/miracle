@extends("admin.admin_app")

@section("content")
        
        <!-- Start content -->
        <div class="content">
            <div class="container">
				<!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Pay Junction Customers</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
                            </li>                            
                            <li class="active">
                               Pay Junction Customers
                            </li>
                        </ol>
                    </div
                </div>
				    <div class="row">
                        @if(Session::has('flash_message'))
                                <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('flash_message') }}
                                </div>
                            @endif
                             
                            <div class="col-lg-12">
                                    
                                    <div class="table-responsive">
                                        <table class="table table-hover mails m-0 table table-actions-bar">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>First Name</th>
                                                    <th>Email</th>                                                     
                                                    <th>Store Number</th>                                                     
                                                    <th>PayJunction ID</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
												@php($i=0)
                                                @foreach($allcustomers as $customer)
                                                <tr>
                                                    
                                                    <td>
														{{$customer->id}}
                                                    </td>
                                                    <td>
														{{$customer->first_name}}
                                                    </td>
                                                    <td>
														{{$customer->email}}
                                                    </td>
                                                    <td>
														{{$customer->store_number}}
                                                    </td>
                                                    <td>
														[PAY_JUNCTION_ID_HERE] {{$pjcustomer_ids[$i++]}}
                                                    </td>
                                                </tr>
												
                                                @endforeach                                              
                                                 
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                            </div> <!-- end col -->

                            
                        </div>

                    <script type="text/javascript">
                    $(document).ready(function() {
                    $('#action-checkbox').click(function(event) {  //on click 
                        if(this.checked) { // check select status
                            $('.checkbox1').each(function() { //loop through each checkbox
                                this.checked = true;  //select all checkboxes with class "checkbox1"               
                            });
                        }else{
                            $('.checkbox1').each(function() { //loop through each checkbox
                                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                            });         
                        }
                    });
                    
                });
                    </script>
             </div>
         </div>    

@endsection
