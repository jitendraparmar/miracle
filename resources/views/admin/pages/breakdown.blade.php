@extends("admin.admin_app")

@section("content")
        
        <!-- Start content -->
        <div class="content">
            <div class="container">
				<!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Customers</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
                            </li>                            
                            <li class="active">
                                This Month
                            </li>
                        </ol>
                    </div
                </div>
				    <div class="row">
                        @if(Session::has('flash_message'))
                                <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('flash_message') }}
                                </div>
                            @endif
                             
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h3 class="text-dark"><?php echo date('F') ?> <?php echo date('Y') ?></h3>
                                        </div>
                                        <div class="col-sm-6">
                                            
                                              <div class="btn-group dropdown-btn-group pull-right">
                                                <a href="{{ URL::to('admin/customers/export_excel') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a">Excel</a>

                                                <a href="{{ URL::to('admin/customers/export_csv') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a">CSV</a>

                                                <a href="{{ URL::to('admin/customers/export_pdf') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a">PDF</a>
                                            </div>                           
                                        </div>
                                    </div>
                                    
                                    <div class="table-responsive">
                                        <table class="table table-hover mails m-0 table table-actions-bar">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Store</th>
                                                    <th>Name</th>
                                                    <th>Plan</th>                                                     
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                            	@foreach($alltransactions as $transactions)
                                                <tr>
                                                	<td>
                                                		{{date('F d, Y', strtotime($transactions->created))}}
                                                	</td>                                                    
                                                    <td>
                                                    	{{$transactions->store_name}}
                                                    </td>
                                                    <td>
                                                        {{$transactions->first_name}} {{$transactions->last_name}}
                                                    </td>
                                                    <td>
                                                    	{{$transactions->product_selection}}
                                                    </td>
                                                    <td>
                                                        {{$transactions->amount}}  
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>   
                                        </table>
                                    </div>
                                </div>
                                
                            </div> <!-- end col -->

                            
                        </div>

             </div>
         </div>    

@endsection
