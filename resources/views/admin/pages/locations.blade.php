@extends("admin.admin_app")

@section("content")
        
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Locations</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
                            </li>                            
                            <li class="active">
                                Locations
                            </li>
                        </ol>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                              
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="m-b-30">
                                            
                                            <a class="pull-right btn btn-default waves-effect waves-light" href="{{URL::to('admin/locations/addlocation')}}"><i class="fa fa-plus"></i> Add Location</a>
                                        </div>
                                    </div>
                                </div>
                             <p>&nbsp;</p>

                            @if(Session::has('flash_message'))
                                <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('flash_message') }}
                                </div>
                            @endif
                             <p>&nbsp;</p>
                             

                            <table id="user_datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Store name</th>
                                    <th>Store Number</th>
                                    <th>Store City</th>                                    
                                    <th class="text-center" style="width: 10%;">Actions</th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach($locations as $i => $store)
                                    <tr>
                                        <td>{{ $store->store_name }}</td>
                                        <td>{{ $store->store_number }}</td>
                                        <td>{{ $store->store_city}}</td>                                         
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ url('admin/locations/addlocation/'.$store->id) }}" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
                                                
                                                <a href="{{ url('admin/locations/delete/'.$store->id) }}" class="on-default remove-row" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>

                                                
                                            </div>
                                        
                                    </td>
                                        
                                    </tr>
                                   @endforeach
                                
                                
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>

             
             </div>
         </div>    

@endsection