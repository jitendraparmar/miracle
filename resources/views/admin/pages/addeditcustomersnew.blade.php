@extends("admin.admin_app")
@section("content")
<link href="{{ URL::asset('admin_assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('admin_assets/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('admin_assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('admin_assets/css/components-md.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('admin_assets/css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />        
<!-- Start content -->
<div class="content">
	<div class="container">
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">{{ isset($customer->id) ? 'Edit Customer' : 'Add Customer' }}</h4>
				<ol class="breadcrumb">
					<li>
						<a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
					</li>
					<li>
						<a href="{{ URL::to('admin/customers') }}">Customers</a>
					</li>                            
					<li class="active">
						{{ isset($customer->id) ? 'Edit Customer' : 'Add Customer' }}
					</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12" style="z-index:99;">   
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				@if(Session::has('flash_message'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ Session::get('flash_message') }}
				</div>
				@endif
			</div>
			<div class="portlet light bordered" id="{{ isset($customer->id) ? 'form_wizard_2' : 'form_wizard_1' }}">
			<div class="portlet-title">
				<div class="caption">
					<i class=" icon-layers font-red"></i>
					<span class="caption-subject font-red bold uppercase"> Customer setup wizard -
						<span class="step-title"> Step 1 of 4 </span>
					</span>
				</div>
			</div>
			<div class="portlet-body form">
				<!--<form action="{{url('/').'/admin/customers/addcustomers'}}" class="" id="submit_form" method="POST">-->
				@if(!isset($customer->id) )
				{!! Form::open(array('url' => array('admin/customers/addnewcustomer'),'class'=>'','name'=>'customers_form','id'=>'submit_form','role'=>'form','enctype' => 'multipart/form-data')) !!}
				@else
				{!! Form::open(array('url' => array('admin/customers/addnewcustomer'),'class'=>'','name'=>'customers_form','id'=>'edit_form','role'=>'form','enctype' => 'multipart/form-data')) !!}
				@endif
				<div class="form-wizard">
					<div class="form-body">
						<ul class="nav nav-pills nav-justified steps">
							<li>
								<a href="#tab1" data-toggle="tab" class="step">
									<span class="number"> 1 </span>
									<span class="desc">
										<i class="fa fa-check"></i> Profile Setup </span>
								</a>
							</li>
							<li>
								<a href="#tab2" data-toggle="tab" class="step active">
									<span class="number"> 2 </span>
									<span class="desc">
										<i class="fa fa-check"></i> Product Setup </span>
								</a>
							</li>
							<li>
								<a href="#tab3" data-toggle="tab" class="step">
									<span class="number"> 3 </span>
									<span class="desc">
										<i class="fa fa-check"></i> Shipping Setup </span>
								</a>
							</li>
							<li>
								<a href="#tab4" data-toggle="tab" class="step active">
									<span class="number"> 4 </span>
									<span class="desc">
										<i class="fa fa-check"></i> Billing Setup </span>
								</a>
							</li>
						</ul>
						<div id="bar" class="progress progress-striped" role="progressbar">
							<div class="progress-bar progress-bar-success"> </div>
						</div>
						<div class="tab-content">
							<div class="alert alert-danger display-none">
								<button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
							<div class="alert alert-success display-none">
								<button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
							<div class="tab-pane active" id="tab1">
								<h3 class="block">Customer's personal details</h3>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label col-md-6">Customers Authorization Status<span class="required"> * </span></label>
											<div class="col-md-6">
												<div class="radio-list">
													<label class="radio-inline">
														<input type="radio" name="customer_auth_status" value="1" data-title="Active" @if(isset($customer->customer_auth_status) and  $customer->customer_auth_status=='1') checked @else checked @endif /> Active </label>
													<label class="radio-inline">
														<input type="radio" name="customer_auth_status" value="0" data-title="Cancel" @if(isset($customer->customer_auth_status) and  $customer->customer_auth_status=='0') checked @endif /> Deactivated </label>
												</div>
												<div id="form_gender_error"> </div>
											</div>
										</div>
									</div>
									<div class="col-sm-6 col-sm-offset-6">&nbsp;</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-lebel">First Name <span class="text-danger">*</span></label>
											<input type="text" name="first_name" class="form-control" placeholder="" value="{{ isset($customer->first_name) ? $customer->first_name : null }}">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Last Name <span class="text-danger">*</span></label>
											<input type="text" name="last_name" class="form-control" placeholder="" value="{{ isset($customer->last_name) ? $customer->last_name : null }}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group m-b-20">
											<label>Date of Birth</label>
											<input type="text" class="form-control" placeholder="mm/dd/yyyy" name="birth_date" id="datepicker" value="{{ (isset($customer->birth_date) && ($customer->birth_date != '0000-00-00')) ? date('m/d/Y',strtotime($customer->birth_date)) : null }}">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group m-b-20">
											<label>E-mail Address <span class="text-danger">*</span></label>
											<input type="text" name="email" class="form-control" placeholder="" value="{{ isset($customer->email) ? $customer->email : null }}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group m-b-20">
											<label>Gender <span class="text-danger">*</span></label><br>
											<div class="radio radio-inline">
												<input type="radio" id="inlineRadio4" value="male" name="gender" @if(isset($customer->gender) and  $customer->gender=='male') checked @else checked @endif>
													   <label for="inlineRadio4"> Male </label>
											</div>
											<div class="radio radio-inline">
												<input type="radio" id="inlineRadio5" value="female" name="gender" @if(isset($customer->gender) and  $customer->gender=='female') checked @endif>
													   <label for="inlineRadio5"> Female </label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab2">
								<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>PRODUCT SELECTION</b></h5>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<div class="radio radio-inline radio-single col-md-7">
												<input type="radio" aria-label="Single radio Two" name="product_selection" id="radio1" value="14.95" class="product-radio" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='14.95') checked @endif>
													   <label for="radio1">Protection+ Supplemental Loss & Damage</label>
											</div>
											<div class="col-md-3">
												<p class="text-left">$14.95 / month</p>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<div class="radio radio-inline radio-single col-md-7">
												<input type="radio" aria-label="Single radio Two" name="product_selection" id="radio2" value="21.95" class="product-radio" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='21.95') checked @endif>
													   <label for="radio2">Secure Plan Supplemental Repair Warranty</label>
											</div>
											<div class="col-md-3">
												<p class="text-left">$21.95 / month</p>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<div class="radio radio-inline radio-single col-md-7">
												<input type="radio" aria-label="Single radio Two" name="product_selection" id="radio3" class="product-radio" value="14.45" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='14.45') checked @endif>
													   <label for="radio3">SINGLE AID-Secure Supplemental Repair Warranty</label>
											</div>
											<div class="col-md-3">
												<p class="text-left">$14.45 / month</p>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<div class="radio radio-inline radio-single col-md-7">
												<input type="radio" aria-label="Single radio Two" name="product_selection" id="radio4" class="product-radio" value="49.95" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='49.95') checked @endif>
													   <label for="radio4">Secure+ Plan Supplemental Repair Warranty</label>
											</div>
											<div class="col-md-3">
												<p class="text-left">$49.95 / month</p>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<div class="radio radio-inline radio-single col-md-7">
												<input type="radio" aria-label="Single radio Two" name="product_selection" id="radio5" class="product-radio" value="30.78" id="singleRadio2" @if(isset($customer->product_selection) and  $customer->product_selection=='30.78') checked @endif>
													   <label for="radio5">SINGLE AID-Secure+ Plan Supplemental Repair Warranty</label>
											</div>
											<div class="col-md-3">
												<p class="text-left">$30.78 / month</p>
											</div>
										</div>
									</div>
								</div>
								<div class="clearfix">&nbsp;</div>
								<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>HEARING AID INFORMATION</b></h5>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group m-b-20">
											<label>Store <span class="text-danger">*</span></label>
											@if(Auth::user()->usertype=='Admin')
											<select name="store_number" class="form-control select-store" data-live-search="true"  data-style="btn-white">
												<option value="">Select One</option>
												@foreach($locations as $location)
												<option value="{{$location->store_number}}" @if(isset($customer->store_number) && $location->store_number==$customer->store_number) selected @endif>{{$location->store_name}}</option>
												@endforeach
											</select>
											<input type="hidden" name="store_name" id="hidden-store" value="">
											@else
											<?php $location_id = Auth::User()->location_id; ?>
											{{ \App\Location::getStoreInfo($location_id)->store_number}}
											<input type="hidden" name="store_name" value="{{ isset($customer->store_name) ? $customer->store_name : \App\Location::getStoreInfo($location_id)->store_name }}">
											<input type="hidden" name="store_number" class="form-control" placeholder="" value="{{ isset($customer->store_number) ? $customer->store_number : \App\Location::getStoreInfo($location_id)->store_number }}">
											@endif
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="">Hearing Specialist<span class="text-danger"> * </span></label>
											<input type="text" class="form-control" name="hearing_specialist_name"  value="{{ isset($customer->hearing_specialist_name) ? $customer->hearing_specialist_name : null }}" />
											<!--<span class="help-block"> Customer's Contact Number </span>-->
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="">Sycle ID #<span class="text-danger"> * </span></label>
											<input type="text" class="form-control" name="sycle_id"  value="{{ isset($customer->sycle_id) ? $customer->sycle_id : null }}" />
											<!--<span class="help-block"> Customer's Post Code </span>-->
										</div>
									</div>
								</div>
								<div class="row">&nbsp;</div>
								<div class="row aid-select-radio-row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2">Select AID<span class="required"> * </span></label>
											<div class="col-sm-4 col-md-3">
												<div class="radio-list" style="margin-left: 30px;">
													<label class="radio-inline">
														<input type="radio" name="aid_type" value="L" class="aid-radio" data-title="Left" @if(isset($customer->aid_type) and  $customer->aid_type=='L') checked @endif /> <p>Left</p> </label>
													<label class="radio-inline">
														<input type="radio" name="aid_type" value="R" class="aid-radio" data-title="Right" @if(isset($customer->aid_type) and  $customer->aid_type=='R') checked @endif /> <p>Right</p> </label>
												</div>
												<div id="form_gender_error"> </div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="col-sm-6 left-aid-form">
											<h5 class="m-t-30"><b>Left Hearing Aid</b></h5>
											<div class="form-group">
												<label class="">Warranty Expiration<span class="text-danger"> * </span></label>
												<input type="text" class="form-control" name="id_warranty_exp_left_aid" id="id_warranty_exp_left_aid" placeholder="mm/dd/yyyy" value="{{ isset($customer->id_warranty_exp_left_aid) ? date('m/d/Y',$customer->id_warranty_exp_left_aid) : null }}" />
												<!--<span class="help-block"> Customer's Contact Number </span>-->
											</div>
											<div class="form-group">
												<label class="">Serial #<span class="text-danger"> * </span></label>
												<input type="text" class="form-control" name="serial_number_left_aid"  value="{{ isset($customer->serial_number_left_aid) ? $customer->serial_number_left_aid : null }}" />
												<!--<span class="help-block"> Customer's Contact Number </span>-->
											</div>
											<div class="form-group">
												<label>Battery Size</label>
												<select class="form-control select2" name="battery_size_left_aid">
													<option value="0">Select One...</option>
													<option value="10" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='10') selected @endif>10</option>
													<option value="13" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='13') selected @endif>13</option>
													<option value="312" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='312') selected @endif>312</option>
													<option value="675" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='675') selected @endif>675</option>
												</select>
											</div>
											<div class="form-group">
												<label class="pull-left">Need Rechargeable</label>
												<div class="col-sm-4 col-md-3">
													<div class="radio-list" style="margin-left: 30px;">
														<label class="radio-inline">
															<input type="radio" name="is_rechargeable_left_aid" value="0" class="aid-radio" data-title="No" checked="" @if(isset($customer->is_rechargeable_left_aid) and  $customer->is_rechargeable_left_aid == 0) checked @endif /> <p>No</p> </label>
														<label class="radio-inline">
															<input type="radio" name="is_rechargeable_left_aid" value="1" class="aid-radio" data-title="Yes" @if(isset($customer->is_rechargeable_left_aid) and  $customer->is_rechargeable_left_aid == 1) checked @endif /> <p>Yes</p> </label>
													</div>
													<div id="form_gender_error"> </div>
												</div>
											</div>
										</div>
										<div class="col-sm-6 right-aid-form">
											<h5 class="m-t-30"><b>Right Hearing Aid</b></h5>
											<div class="form-group">
												<label>Warranty Expiration </label>
												<input type="text" name="id_warranty_exp_right_aid" id="id_warranty_exp_right_aid" class="form-control" placeholder="mm/dd/yyyy" value="{{ isset($customer->id_warranty_exp_right_aid) ? date('m/d/Y',$customer->id_warranty_exp_right_aid) : null }}">
											</div>
											<div class="form-group">
												<label class="">Serial #<span class="text-danger"> * </span></label>
												<input type="text" class="form-control" name="serial_number_right_aid"  value="{{ isset($customer->serial_number_right_aid) ? $customer->serial_number_right_aid : null }}" />
												<!--<span class="help-block"> Customer's Contact Number </span>-->
											</div>
											<div class="form-group">
												<label>Battery Size</label>
												<select class="form-control select2" name="battery_size_right_aid">
													<option value="0">Select One...</option>
													<option value="10" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='10') selected @endif>10</option>
													<option value="13" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='13') selected @endif>13</option>
													<option value="312" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='312') selected @endif>312</option>
													<option value="675" @if(isset($customer->battery_size_left_aid) and  $customer->battery_size_left_aid=='675') selected @endif>675</option>
												</select>
											</div>
											<div class="form-group">
												<label class="pull-left">Need Rechargeable</label>
												<div class="col-sm-4 col-md-3">
													<div class="radio-list" style="margin-left: 30px;">
														<label class="radio-inline">
															<input type="radio" name="is_rechargeable_right_aid" value="0" class="aid-radio" data-title="No" checked="" @if(isset($customer->is_rechargeable_right_aid) and  $customer->is_rechargeable_right_aid == 0) checked @endif /> <p>No</p> </label>
														<label class="radio-inline">
															<input type="radio" name="is_rechargeable_right_aid" value="1" class="aid-radio" data-title="Yes" @if(isset($customer->is_rechargeable_right_aid) and  $customer->is_rechargeable_right_aid == 1) checked @endif /> <p>Yes</p> </label>
													</div>
													<div id="form_gender_error"> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab3">
								<h3 class="block">Customer's shipping details</h3>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="">Street address
												<span class="text-danger"> * </span>
											</label>
											<input type="text" class="form-control" name="address" value="{{ isset($customer->address) ? $customer->address : null }}"/>
											<!--<span class="help-block"> Customer's address </span>-->
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="">City<span class="text-danger"> * </span></label>
											<input type="text" class="form-control" name="city"  value="{{ isset($customer->city) ? $customer->city : null }}" />
											<!--<span class="help-block"> Customer's City </span>-->
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>State <span class="text-danger">*</span></label>
											<select name="state" class="form-control" data-live-search="true"  data-style="btn-white">
												<option value="">Select One</option>
												@foreach($allstate as $state)
												<option value="{{$state->id}}" @if(isset($customer->state) && $state->id==$customer->state) selected @endif>{{$state->state_name}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="">Contact Number<span class="text-danger"> * </span></label>
											<input type="text" class="form-control" name="telephone_number"  value="{{ isset($customer->telephone_number) ? $customer->telephone_number : null }}" />
											<!--<span class="help-block"> Customer's Contact Number </span>-->
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="">Country
												<span class="text-danger"> * </span>
											</label>
											<input type="text" class="form-control" name="country" value="{{ isset($customer->country) ? $customer->country : 'United States' }}" readonly/>
											<!--<span class="help-block"> Customer's Country </span>-->
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="">Post Code<span class="text-danger"> * </span></label>
											<input type="text" class="form-control" name="post_code"  value="{{ isset($customer->post_code) ? $customer->post_code : null }}" />
											<!--<span class="help-block"> Customer's Post Code </span>-->
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab4">
								<div class="row">
									<div class="col-sm-8">
										<div class="form-group col-sm-12">
											<h5 class="text-muted text-uppercase m-t-0 m-b-20">You have selected: </5>
												<label class="selected-product-class"></label>
												<hr>
												<!--<span class="help-block"> Customer's Post Code </span>-->
												</div>
												</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															<label class="control-label col-sm-2" style="font-weight: 700;">Recurring Frequency<span class="required"> * </span></label>
															<div class="col-sm-4">
																<div class="radio-list" style="margin-left: 30px;">
																	<label class="radio-inline">
																		<input type="radio" name="billing_frequency" value="1" class="plan-radio" data-title="Monthly" @if(isset($customer->plan_type) and  $customer->plan_type=='1') checked @else checked @endif /> Monthly </label>
																	<!--<input type="text" class="form-control" value="" placeholder="Months">-->
																	<label class="radio-inline">
																		<input type="radio" name="billing_frequency" value="2" class="plan-radio" data-title="Yearly" @if(isset($customer->plan_type) and  $customer->plan_type=='2') checked @endif /> Yearly </label>
																	<!--<input type="text" class="form-control" value="" placeholder="Years">-->
																</div>
																<div id="form_gender_error"> </div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															<div class="col-sm-2">&nbsp;</div>
															<div class="col-sm-10">
																<div class="col-sm-3 col-md-2" style="width: 10%; margin-top: 10px;">
																	<label class="control-label product-val pull-left"></label>
																</div>
																<div class="col-sm-5 col-md-3">
																	<input type="text" name="monthly_duration" class="form-control duration monthly-plan " value="" placeholder="Upto 48 months" max="48">
																	<input type="text" name="yearly_duration" class="form-control duration yearly-plan" value="" placeholder="Upto 4 years" max="4">
																</div>
																<div class="col-sm-5 col-md-4" style="margin-top: 10px;">
																	<span class="total-cost"></span>
																</div>	
																<div id="form_gender_error"> </div>
															</div>
														</div>
													</div>
												</div>
												<div class="clearfix">&nbsp;</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="col-sm-4 pull-left">Billing Date
																<span class="text-danger"> * </span>
															</label>
															<div class="billing-date col-sm-3"></div><div class="repeatation"></div>
															<!--<span class="help-block"> Customer's Country </span>-->
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															&nbsp;
														</div>
													</div>
												</div>
												<!--</div>-->
												<div class="row">
													<div class="col-sm-12"> 
														<ul class="nav nav-tabs tabs">
															<li class="active tab sub-tab">
																<a href="#card" data-toggle="tab" aria-expanded="false"> 
																	<span class="visible-xs"><i class="fa fa-home"></i></span> 
																	<span class="hidden-xs">Card</span> 
																</a> 
															</li> 
															<li class="tab sub-tab"> 
																<a href="#check" data-toggle="tab" aria-expanded="false"> 
																	<span class="visible-xs"><i class="fa fa-user"></i></span> 
																	<span class="hidden-xs">Check</span> 
																</a> 
															</li>
														</ul>
														<div class="tab-content"> 
															<div class="tab-pane active" id="card">
																<div class="card-box">
																	<!--<form action="admin/customers/processpayments" class="" name="customer_payment_form" id="customer_payment_form" role="form" enctype="multipart/form-data">-->
																	<h5 class="text-muted text-uppercase m-t-0 m-b-20" style="display: inline;margin-right: 50px;"><b>PAYMENT INFORMATION</b></h5>
																	<br /><br />
																	<input type="hidden" name="id" value="{{ isset($customer->id) ? $customer->id : null }}">
																	<input type="hidden" name="type" value="charge">
																	<div class="row">
																		<div class="col-sm-6">
																			<div class="form-group">
																				{!! Form::label(null, 'Credit card number:') !!}
																				{!! Form::text('cardNumber', null, ['class' => 'form-control']) !!}
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				{!! Form::label(null, 'CVV:') !!}
																				{!! Form::text('cardCvv', null, ['class' => 'form-control']) !!}
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-sm-6">
																			<div class="form-group">
																				{!! Form::label(null, 'Ex. Month') !!}
																				{{ Form::selectMonth('cardExpMonth', 7, ['class' => 'form-control']) }}
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group">
																				{!! Form::label(null, 'Ex. Year') !!}
																				{!! Form::selectYear('cardExpYear', date('Y'), date('Y') + 10, null, ['class' => 'form-control']) !!}
																			</div>
																		</div>
																	</div>
																	<div class="form-group">
																		<!--  {-- Form::select('type', array('charge' => 'Charge & Update', 'update' => 'Update Only')); --} -->
																	</div>
																	<div class="form-group">
																		<!--{!! Form::submit('Submit', ['class' => 'btn btn-primary btn-order', 'id' => 'ccsubmitBtn', 'style' => 'margin-bottom: 10px;']) !!}-->
																	</div>
																	<!--</form>-->
																</div>
															</div>
															<div class="tab-pane" id="check">
																<div class="card-box">
																	{!! Form::open(array('url' => array('admin/customers/achprocesspayments'),'class'=>'','name'=>'customer_payment_form','id'=>'customer_payment_form','role'=>'form','enctype' => 'multipart/form-data')) !!}
																	<h5 class="text-muted text-uppercase m-t-0 m-b-20" style="display: inline;margin-right: 50px;"><b>ACH PAYMENT INFORMATION</b></h5>
																	<br /><br />

																	<input type="hidden" name="id" value="{{ isset($customer->id) ? $customer->id : null }}">
																	<input type="hidden" name="type" value="charge">

																	<div class="row">
																		<div class="col-sm-4">
																			<div class="form-group">
																				{!! Form::label(null, 'Routing Number:') !!}
																				{!! Form::text('routingNumber', null, ['class' => 'form-control']) !!}
																			</div>
																		</div>
																		<div class="col-sm-4">
																			<div class="form-group">
																				{!! Form::label(null, 'Account Number') !!}

																				{!! Form::text('accNumber', null, ['class' => 'form-control']) !!}

																			</div> 
																		</div>
																	</div>
																	<div class="form-group">
																		<!--  {-- Form::select('type', array('charge' => 'Charge & Update', 'update' => 'Update Only')); --} -->
																	</div>

																	{!! Form::close() !!}
																</div>
															</div>
														</div> <!-- end col -->
													</div>
												</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<a href="javascript:;" class="btn default button-previous">
												<i class="fa fa-angle-left"></i> Back </a>
											<a href="javascript:;" class="btn btn-outline green button-next"> Continue
												<i class="fa fa-angle-right"></i>
											</a>
											<input type="submit" class="btn green button-submit" value="Submit">
										</div>
									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
			<div class="col-lg-12">&nbsp;</div>
		</div>
	</div>
</div>
<style>
	.form-wizard .steps, .form-wizard .steps>li>a.step
	{
		background-color: #fff !important;
	}
	.form-wizard .steps>li>a.step .number{
		padding: 5px 15px 13px;
	}
	.form-wizard .steps>li.active>a.step .number{
		background-color: #5fbeaa;
	}
	label{
		font-weight: 700;
	}
	.selected-product-class i
	{
		color: #5fbeaa ;
	}
	.billing-text
	{
		padding-top: 7px;
	}
	li.sub-tab.active{
		border-bottom: 2px solid #5fbeaa;
	}
</style>
<script src="{{ URL::asset('admin_assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
<script src="{{ URL::asset('admin_assets/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script src="{{ URL::asset('admin_assets/plugins/jquery-validation/js/additional-methods.js') }}"></script>
<script src="{{ URL::asset('admin_assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ URL::asset('admin_assets/js/app.min.js') }}"></script>
<script src="{{ URL::asset('admin_assets/pages/scripts/form-wizard.js') }}"></script>
<script src="{{ URL::asset('admin_assets/layouts/layout/scripts/layout.min.js') }}"></script>
<script src="{{ URL::asset('admin_assets/js/customer.js') }}"></script>
@endsection
