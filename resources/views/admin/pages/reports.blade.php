@extends("admin.admin_app")
@section("content")
<!-- Start content -->
<div class="content">
	<div class="container">
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title">Customers</h4>
				<ol class="breadcrumb">
					<li>
						<a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
					</li>                            
					<li class="active">
						Customers
					</li>
				</ol>
			</div
		</div>
		<div class="row">
			@if(Session::has('flash_message'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ Session::get('flash_message') }}
			</div>
			@endif
			<div class="col-lg-12">
				<div class="card-box">
					<div class="row">
						<div class="col-sm-6">
							<form role="form">
								<div class="form-group contact-search col-sm-4">
									<input type="text" id="search" class="form-control" placeholder="Search...">
									<button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
								</div> <!-- form-group -->
							</form>
							<div class="col-sm-4">
								<select class="form-control" id="bydate" name="bydate">
									<option value="all">Select</option>
									<option value="1" <?= isset($bydate) && $bydate == 1 ? 'selected' : '' ?>>Daily sales</option>
									<option value="2" <?= isset($bydate) && $bydate == 2 ? 'selected' : '' ?>>Month to date</option>
									<option value="3" <?= isset($bydate) && $bydate == 3 ? 'selected' : '' ?>>Year to date</option>
								</select>
							</div>
							<div class="col-sm-4">
								<?php if(session()->get('usertype') == 'Admin'){ ?>
								<select class="form-control" id="bystore" name="bystore">
									<option value="all">Select</option>
									@foreach($stores as $store)
										@if($store->store_number == $bystore)
										<option value="{{$store->store_number}}" selected="">{{$store->store_name}}</option>
										@else
										<option value="{{$store->store_number}}">{{$store->store_name}}</option>
										@endif
									@endforeach
								</select>
								<?php } ?>
							</div>
						</div>
						<div class="col-sm-6">
							<a href="{{ URL::to('admin/customers/addcustomers') }}" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal" 
							   data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> Add Customer</a>
							<div class="btn-group dropdown-btn-group pull-right">
								<a href="javascript:;" class="btn btn-default btn-md waves-effect waves-light m-b-30" id="exportexcel">Excel</a>
								<a href="javascript:;" class="btn btn-default btn-md waves-effect waves-light m-b-30" id="exportcsv">CSV</a>
								<a href="javascript:;" class="btn btn-default btn-md waves-effect waves-light m-b-30" id="exportpdf">PDF</a>
							</div>                           
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover mails m-0 table table-actions-bar">
							<thead>
								<tr>
									<th>
							<div class="checkbox checkbox-primary checkbox-single m-r-15">
								<input id="action-checkbox" type="checkbox">
								<label for="action-checkbox"></label>
							</div>
							</th>
							<th>Name</th>
							<th>Product</th>
							<th>Status</th>
							<th>Email</th>                                                     
							<th>Is battery rechargeable(L/R)</th>
							<th>Action</th>
							</tr>
							</thead>
							<tbody>
								@foreach($allcustomers as $customers)
								<tr>
									<td>
										<div class="checkbox checkbox-primary m-r-15">
											<input type="hidden" name="_token" value="{!! csrf_token() !!}">
											<input class="checkbox1" id="checkbox2" name="chk_customer" value="{{$customers->id}}" type="checkbox">
											<label for="checkbox2"></label>
										</div>
									</td>
									<td>
										{{$customers->name}}
									</td>
									<td>
										@if($customers->product_selection=='14.95') Protection+ Supplemental Loss & Damage @endif
										@if($customers->product_selection=='21.95') Secure Plan Supplemental Repair Warranty @endif
										@if($customers->product_selection=='14.45') SINGLE AID-Secure Supplemental Repair Warranty @endif
										@if($customers->product_selection=='49.95') Secure+ Plan Supplemental Repair Warranty @endif
										@if($customers->product_selection=='30.78') SINGLE AID-Secure+ Plan Supplemental Repair Warranty @endif
									</td>
									<td>
										@if($customers->customer_auth_status==1)
										Active
										@else
										Canceled
										@endif
									</td>
									<td>
										{{$customers->email}}
									</td>
									<td>
										{{$customers->is_rechargeable_left_aid.' / '.$customers->is_rechargeable_right_aid}}
									</td>
									<td>
										<a href="{{ url('admin/customers/edit/'.$customers->id) }}" class="table-action-btn"><i class="md md-edit"></i></a>
										<a href="{{ url('admin/customers/delete/'.$customers->id) }}" class="table-action-btn"><i class="md md-close"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div> <!-- end col -->
		</div>

		<script type="text/javascript">
			$(document).ready(function () {
				$('#action-checkbox').click(function (event) {  //on click 
					if (this.checked) { // check select status
						$('.checkbox1').each(function () { //loop through each checkbox
							this.checked = true;  //select all checkboxes with class "checkbox1"               
						});
					} else {
						$('.checkbox1').each(function () { //loop through each checkbox
							this.checked = false; //deselect all checkboxes with class "checkbox1"                       
						});
					}
				});
				
				// Sorting by date
				$('#bydate').change(function(){
					var bydate = $('#bydate').val();
					var bystore = $('#bystore').val();
					window.location = APP_URL + '/admin/reports/'+bydate+'/'+bystore;
				});
				
				// Sorting by store
				$('#bystore').change(function(){
					var bydate = $('#bydate').val();			
					var bystore = $('#bystore').val();
					window.location = APP_URL + '/admin/reports/'+bydate+'/'+bystore;
				});
				
				// Export excel
				$('#exportexcel').click(function(){
					var bydate = $('#bydate').val();			
					var bystore = $('#bystore').val();
					var dataurl = APP_URL + '/admin/customers/export_excel';
					var chk_customer = [];
					$.each($("input[name='chk_customer']:checked"), function(){            
						chk_customer.push($(this).val());
					});
					sendajax(bydate,bystore,chk_customer,dataurl);
				});
				//End export excel
				
				// Export csv
				$('#exportcsv').click(function(){
					var bydate = $('#bydate').val();			
					var bystore = $('#bystore').val();
					var dataurl = APP_URL + '/admin/customers/export_csv';
					var chk_customer = [];
					$.each($("input[name='chk_customer']:checked"), function(){            
						chk_customer.push($(this).val());
					});
					sendajax(bydate,bystore,chk_customer,dataurl);
				});
				//End export csv
				
				// Export pdf
				$('#exportpdf').click(function(){
					var bydate = $('#bydate').val();			
					var bystore = $('#bystore').val();
					var dataurl = APP_URL + '/admin/customers/export_pdf';
					var chk_customer = [];
					$.each($("input[name='chk_customer']:checked"), function(){            
						chk_customer.push($(this).val());
					});
					sendajax(bydate,bystore,chk_customer,dataurl);
				});
				//End export pdf
				
				function sendajax(bydate,bystore,chk_customer,dataurl)
				{
					$.ajax({
						headers:
						{
							'X-CSRF-Token': $('input[name="_token"]').val()
						},
						url: dataurl,
						type: 'POST',
						data: {bydate:bydate,bystore:bystore,chk_customer:chk_customer},
						async: false,
						success: function (data) {
							if(data.file != '')
							{
								var link=document.createElement('a');
									link.href=data.fullurl;
									link.target= '_blank';
									link.download=data.file;
									link.click();
							}
							else
							{
								alert('Opps something went wrong please try again.');
								return false;
							}
						},
						cache: false
				    });
				}
			});
		</script>
	</div>
</div>
@endsection