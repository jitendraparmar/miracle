@extends("admin.admin_app")

@section("content")
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <!-- Start content -->
        <div class="content">
            <div class="container">
				<!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Customer Cancellation Requests</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
                            </li>                            
                            <li class="active">
                                Customer Cancellation Requests
                            </li>
                        </ol>
                    </div
                </div>
				    <div class="row">
                        @if(Session::has('flash_message'))
                                <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('flash_message') }}
                                </div>
                            @endif
                             
                            <div class="col-lg-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <form role="form">
                                                <div class="form-group contact-search m-b-30">
                                                    <input type="text" id="search" class="form-control" placeholder="Search...">
                                                    <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                                </div> <!-- form-group -->
                                            </form>
                                        </div>
                                    </div>
                                    
                                    <div class="table-responsive">
                                        <table class="table table-hover mails m-0 table table-actions-bar">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="checkbox checkbox-primary checkbox-single m-r-15">
                                                            <input id="action-checkbox" type="checkbox">
                                                            <label for="action-checkbox"></label>
                                                        </div>
                                                         
                                                    </th>
                                                    <th>Name</th>
                                                    <th>User Status</th>
                                                    <th>Request Status</th>
                                                    <th>Email</th>                                                     
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                @foreach($allrequests as $customers)
                                                <tr>
                                                    <td>
                                                        <div class="checkbox checkbox-primary m-r-15">
                                                            <input class="checkbox1" id="checkbox2" type="checkbox">
                                                            <label for="checkbox2"></label>
                                                        </div>
                                                        
                                                         
                                                    </td>
                                                    
                                                    <td>
                                                        {{$customers->first_name}} {{$customers->last_name}}
                                                    </td>
                                                    <td>
                                                          
                                                            @if($customers->customer_auth_status==1)
                                                                Active
                                                            @else
                                                                Inactive
                                                            @endif
                                                         
                                                    </td>
                                                    <td>
                                                          
                                                            @if($customers->status==1)
																Completed
                                                            @else
                                                                Pending
                                                            @endif
                                                         
                                                    </td>
                                                    
                                                    <td>
                                                        {{$customers->email}}
                                                    </td>
                                                    
                                                     
                                                    <td>
														@if($customers->status==0)
                                                            <a href="javascript:;" class="btn btn-primary processed" data-bind="{{$customers->id}}" id="">Approve Request</a>
														@else
															--
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                                
                                                 
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                            </div> <!-- end col -->

                            
                        </div>

                    <script type="text/javascript">
                    $(document).ready(function() {
                    $('#action-checkbox').click(function(event) {  //on click 
                        if(this.checked) { // check select status
                            $('.checkbox1').each(function() { //loop through each checkbox
                                this.checked = true;  //select all checkboxes with class "checkbox1"               
                            });
                        }else{
                            $('.checkbox1').each(function() { //loop through each checkbox
                                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                            });         
                        }
                    });
                    
                });
                    </script>
             </div>
         </div>    
<script src="{{ URL::asset('admin_assets/js/cancelrequest.js') }}"></script>
@endsection
