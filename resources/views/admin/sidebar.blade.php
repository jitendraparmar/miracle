<div class="left side-menu">
  <div class="sidebar-inner slimscrollleft"> 
    <div id="sidebar-menu">
      <ul>
        <li class="text-muted menu-title">Navigation</li>
        @if(Auth::user()->usertype=='Admin')

        <li> <a href="{{ URL::to('admin/dashboard') }}" class="waves-effect {{classActivePath('dashboard')}}"><i class="ti-home"></i> <span> Dashboard </span></a>        
        </li>        
        <li> <a href="{{ URL::to('admin/users') }}" class="waves-effect {{classActivePath('users')}}"><i class="ti-user"></i> <span> Store Admin </span></a>        
        </li>
        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect {{classActivePath('customers')}}"><i class="ti-user"></i> <span> Members </span> <span class="menu-arrow"></span> </a>
          <ul class="list-unstyled">
            <li class="{{classActivePath('customers')}}"><a href="{{ URL::to('admin/customers') }}">Customers</a></li>
            <li class="{{classActivePath('customers')}}"><a href="{{ URL::to('admin/customers/addcustomers') }}">Add Customer</a></li>
          </ul>
        </li>
		<li> <a href="{{ URL::to('admin/cancelrequest') }}" class="waves-effect {{classActivePath('cancelrequest')}}"><i class="ti-user"></i> <span> Cancel Request </span></a>
        </li>
        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect {{classActivePath('locations')}}"><i class="ti-bag"></i> <span> Locations </span> <span class="menu-arrow"></span> </a>
          <ul class="list-unstyled">
            <li class="{{classActivePath('locations')}}"><a href="{{ URL::to('admin/locations') }}">Locations</a></li>
            <li class="{{classActivePath('locations')}}"><a href="{{ URL::to('admin/locations/addlocation') }}">Add Locations</a></li>
          </ul>
        </li>
        <li class="has_sub"> <a href="{{ URL::to('admin/reports') }}" class="waves-effect {{classActivePath('reports')}}"><i class="ti-bar-chart"></i><span> Reports </span> </a> </li>
         <li> <a href="{{ URL::to('admin/customers/addcustomers') }}" class="waves-effect"><i class="ti-shopping-cart"></i> <span> New Order </span></a> </li>
        
        @else
        
         <li> <a href="{{ URL::to('admin/dashboard') }}" class="waves-effect {{classActivePath('dashboard')}}"><i class="ti-home"></i> <span> Dashboard </span></a>        
        </li>
        <li class="has_sub"> <a href="javascript:void(0);" class="waves-effect {{classActivePath('customers')}}"><i class="ti-user"></i> <span> Members </span> <span class="menu-arrow"></span> </a>
          <ul class="list-unstyled">
            <li class="{{classActivePath('customers')}}"><a href="{{ URL::to('admin/customers') }}">Customers</a></li>
            <li class="{{classActivePath('customers')}}"><a href="{{ URL::to('admin/customers/addcustomers') }}">Add Customer</a></li>
          </ul>
        </li>
        <li class="has_sub"> <a href="{{ URL::to('admin/reports') }}" class="waves-effect {{classActivePath('reports')}}"><i class="ti-bar-chart"></i><span> Reports </span> </a> </li>
        <li> <a href="{{ URL::to('admin/customers/addcustomers') }}" class="waves-effect"><i class="ti-shopping-cart"></i> <span> New Order </span></a> </li>
        @endif
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>